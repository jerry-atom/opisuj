import { Link, graphql, useStaticQuery } from "gatsby"
import React from "react"
import Content from "../components/content"
import Header from "../components/header"
import Footer from "../components/footer"
import SEO from "../components/seo"

export default function Home() {
  const data = useStaticQuery(
    graphql`
      query {
        allMarkdownRemark {
          edges {
            node {
              id
              frontmatter {
                category
                authorName
                datePublished
                slug
                title
                writer
              }
            }
          }
        }
      }
    `
  )

  return (
    <>
      <SEO title="Opisuj seminárky, slohovky, maturitní okruhy, rozbory knih..."/>
      <Header>
        <Link to="https://motherfuckingwebsite.com/" target="_blank" rel="noreferrer nofollow" title="O vzhledu stránek">[ O vzhledu stránek ]</Link>
      </Header>
      <Content>
        <h1>Opisuj <small>&hellip;seminárky, slohovky, maturitní okruhy, rozbory knih</small></h1>
        <p>
          Maturuješ? Píšeš seminárku? Potřebuješ rychle něco obšlehnout? Nažhav své Ctrl+C/Ctrl+V a <strong>opisuj.cz</strong>!<br />
              Toto je web se zaměřením na sdílení materiálů ke studiu. Postupně zde budou narůstat nové zdroje.
        </p>
        <p>
          Co za to? Absolutně nic! Všimni si, že tu ani nejsou reklamy. Ale pokud chceš sdílet své materiály, můžeš mi to poslat na e-mail: <a href="mailto:jaroslav.novotny.84@gmail.com">jaroslav.novotny.84@gmail.com</a>, nebo rovnou merge request na <a href="https://gitlab.com/jerry-atom/opisuj" target="_blank" rel="noreferrer nofollow" title="GitLab">GitLabu</a>.
        </p>
        <p>Ale teď už k věci!</p>
        <h2>Literatura</h2>
        <div className="responsive">
          <table>
            <thead>
              <tr>
                <th>Spisovatel</th>
                <th>Dílo</th>
                <th>Zařazení</th>
                <th>Vytvořeno</th>
                <th>Autor</th>
              </tr>
            </thead>
            <tbody>
              {(data.allMarkdownRemark.edges).map(({node}) => (
                <tr key={node.id}>
                  <td>{node.frontmatter.writer}</td>
                  <td className="tappable"><Link to={node.frontmatter.slug}>{node.frontmatter.title}</Link></td>
                  <td>{node.frontmatter.category}</td>
                  <td>{node.frontmatter.datePublished}</td>
                  <td>{node.frontmatter.authorName}</td>
              </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Content>
      <Footer />
    </>
  )
}