---
title: "Romeo a Julie"
writer: "William Shakespeare"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, William Shakespeare, Romeo a Julie"
description: "Čtenářský deník k povinné maturitní četbě hry Romeo a Julie od William Shakespeare"
slug: "/literatura/0003-william-shakespeare-romeo-a-julie/"
authorName: "@sari"
dateCreated: "2020-06-14"
dateModified: "2020-06-14"
dateMublished: "2020-06-14"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## William Shakespeare

### Národnost
Angličan

### Profese
dramatik, básník

### Život
Narodil se 1564 ve Stratfordu nad Avonou a zemřel 1616 tamtéž. Působil v Londýně v herecké společnosti Služebníci lorda komořího (Lord Chamberlain's Men), později Královská společnost (King's Men).

### Další díla
Zkrocení zlé ženy (1594), Sen noci svatojánské (1596), Kupec benátský (1597), Veselé paničky windsorské (1601), Hamlet (1604), Othello (1604), Macbeth (1606), Král Lear (1606)

### Současníci
Thomas Kyd, Christopher Marlow

## Romeo a Julie

### Žánr
lyricko-epická veršovaná tragická hra o pěti dějstvích

### Premiéra hry
1595

### Děj
Odehrává se ve Veroně 16. století. Jsou zde dva rody, které se bytostně nesnáší: Montekové a Kapuleti. Romeo Montek (17 let) a Julie Kapuletová (14 let) se zamilují a ještě téhož dne se nechají tajně oddat. Romeo však druhý den zabije v zápase Juliina bratrance. Za to je vyhnán z Verony a Julie se má vdát za jiného muže. Proto pozře lék, po němž upadá do stavu podobného smrti. Rodina ji pohřbívá a Romeo, aniž by věděl, že Julie žije, polyká jed a umírá. Julie se probouzí u jeho těla a ze žalu si probodne srdce. Rodiče obou rodů se po tragédii usmiřují.

### Postava #1
Romeo Montek (17 let), vzdělaný, pohledný, klidné povahy, má bezstarostný život. Po seznámení s Julií se ocitá v pasti sebeklamu o vlastní dospělosti, jedná zbrkle a pateticky.

### Postava #2
Julie Kapuletová (13 let), pohledná, poslušná ale umíněná, má bezstarostný život. Jakmile v ní Romeo probudí lásku, začne být mistrně intrikánská, manipulativní a odhodlaná.

### Jazyk díla
Překlad od Zdeňka Urbánka - spisovná čeština, absence rýmu, zachovává skladbu veršů a intonaci anglického originálu.

### Hlavní idea díla
Kritika společnosti, která brání lidem ve svobodné cestě za vlastním štěstím, ale také varování před zbrklými rozhodnutími.

### Názor na dílo
Imponovala mi vytříbená forma veršů a skrytá hloubka charakterů postav. Odpuzovala mě zprofanovanost díla, patetický jazyk lásky, místy zbytečné postavy a hloupé chování některých postav.

### Podrobný děj
Dějství se začíná malichernou šarvátkou mezi mladíky Benvoliem z rodu Monteků a Tybaltem z rodu Kapuletů z italské Verony 16. století. Nebýt zásahu vévody Escala, nepokoj by znovu přerostl ve vraždění mezi oběma rody. Vévoda proto pokárá hlavy obou rodů a udělí jim přísné ultimátum, jehož porušení už bude trestat smrtí. Tento společenský kontext má zásadní vliv na vývoj celého příběhu.
Montek pověřuje svého synovce Benvolia, aby zjistil, proč je Montekův syn Romeo tak zádumčivý. Romeo je zamilovaný do  Kapuletovy neteře Rosalindy.
Kapulet pořádá tradiční ples, kde chce seznámit svou dceru Julii s mladým šlechticem Parisem, který má o Julii zájem. Benvolio, Romeo a jeho přítel Mercuzio se náhodou dozvídají o tomto plese a rozhodnou se tam vetřít bez pozvání a trochu se pobavit.
Na plese se Romeo setká s Julií a okamžitě se zamilují. Mezitím je ale Romeo rozpoznán Tybaltem, synovcem Kapuleta. Sám Kapulet však zakáže Tybaltovi mstít se přímo na plese.
Ještě téže noci po plese Romeo pronikne do Kapuletovy zahrady a pod oknem si s Julií otevřeně vyznávají lásku a dohodnou se, že je otec Lorenzo oddá. Romeo se ihned vypraví za Lorenzem. Přemluvit ho k tomu není těžké: otec tak doufá usmířit oba rody. Nakonec oba milence prohlásí za manžele, byť s obavami, aby je po tom nestihl žal.
Následujícího dne se Tybalt chce mstít Romeovi za přítomnost na plese. Najde však Benvolia a Mercuzia, s nímž se začne šermovat. V průběhu zápasu přijde Romeo a snaží se je odtrhnout, protože jsou teď všichni příbuzní. Tybalt ale zabije Mercuzia ještě dřív než Romeo vyzradí tu novinu. Romea zaslepí touha po pomstě za smrt přítele a zabije Tybalta, svého švagra. Neboť porušili vévodovo ultimátum, prchá před očekávaným trestem smrti do Mantovy. Na scéně zůstává Benvolio jako svědek a obhajuje Romea, díky čemuž vévoda rozhodne Romea jen navždy vyhnat z Verony.
Krátce po té, co se Julie dozvídá šokující zprávu o tom, že Romeo zabil jejího bratrance, za ní přichází její otec a oznamuje jí, že ji zaslíbil Parisovi a že svatba bude za pár dní. Je rozhořčen dceřiným odmítáním tak skvělého manžela a nevyslechne Juliiny důvody.
Julie v zoufalství navštíví Lorenza a prosí ho o pomoc. Lorenzo, aby předešel porušení manželského slibu, dává Julii tresť, po jejímž požití má Julie na 42 hodin upadnou do podobnému smrti. Julie tedy v den svatby předstírá smrt, lest se zdaří a je pohřbena do rodinné hrobky Kapuletů vedle Tybalta.
Lorenzo o tomto plánu vyslal Romeovi dopis, ale posel je nešťastnou náhodou zadržen a Romeo tak neví, že Julie není mrtvá. Koupí si silný jed a v noci jde ke hrobce. Zde se střetne s Parisem, který své zesnulé nastávající přinesl květiny, a v zápase šlechtice zbíje. Sám pak ulehne vedle Julie a otráví se. Zanechává po sobě vysvětlující dopis. Julie po procitnutí spatří mrtvého manžela a v zoufalství si jeho dýkou probodne srdce.
Vévoda pak vyslýchá svědky a shledává jejich výpověď shodnou s dopisem. Vše je objasněno. Odebírá se do ústraní rozhodnout o tom, komu a jaký vyměří trest. Kapuletové a Montekové se nad hrobem svých dětí usmiřují.
