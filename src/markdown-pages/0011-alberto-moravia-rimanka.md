---
title: "Římanka"
writer: "Alberto Moravia"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Alberto Moravia, Římanka"
description: "Čtenářský deník k povinné maturitní četbě Alberto Moravia - Římanka"
slug: "/literatura/0011-alberto-moravia-rimanka/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-26"
dateModified: "2021-02-26"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Alberto Moravia

### Vlastní jméno
Alberto Pincherle

### Národnost
Ital

### Profese
novinář, spisovatel, esejista, prozaik, představitel italského neorealismu.

### Život
Vyrůstal v rodině architekta. V dětství onemocněl tuberkulózou kostí. Studoval angličtinu, němčinu, francouzštinu. Ve 30. letech bojoval s tehdejší fašistickou cenzurou, jelikož pocházel z židovské rodiny. Roku 1984 byl zvolen do Evropského parlamentu. Ve svých dílech usiluje o vidění každodenního života s důrazem na sociální problematiku.

### Další díla
**Romány:** Horalka (1957), Lhostejní (1929), Maškaráda ( 1941), Já a on (1971)

**Povídky:** Epidemie, Římské povídky (1954), Automat (1963)

**Drama:** Život je hra

**Publicistické knihy:** Člověk jako cíl, Představa Indie

### Současníci
Italo Calvino, Giovanni Arpino

## Římanka

### Žánr
neorealismus, psychologický román

### Vyšla
1947

### Děj
Hrdinkou a vypravěčkou je velice krásná mladá římská dívka Adriana. Její příběh začíná v 16 letech, kdy na přání své matky pózuje malířům na umělecké akty. V osmnácti poznává Gina. Ten jí slíbil, že si ji vezme, ale později zjistí, že Gino je ženatý a má syna. Po této informaci zrušila zasnoubení. Pak se s ním ještě nadále scházela, ale šlo spíše už jen o sex.

Kamarádka Gisella ji seznámila s Astaritou. Jednoho dne jeli na výlet, kde se Astarita na ni vrhl, dá se říct, že ji znásilnil. Po sexu s ní jí za něj zaplatil. S pocitem spoluviny a dohody peníze přijala.

Postupem času ji chudoba donutí stát se prostitutkou. Provozuje své povolání se sebevědomím, zvědavostí a na duši zůstává neposkvrněná.
Při večerní procházce s Gisellou poznaly mladé muže. Jedním z nich byl  Giacomo (Mino), do kterého se Adriana rázem zamilovala. Ale on ji lásku neopětoval, hrubě si ji dobíral, říkal jí ošklivé věci. Postupem času se do ní zamiloval, i když šlo spíše o závazek sebelásky.
Dalším jejím milencem byl Sonzogno. Byl vrah.

Po nějaké době zjistila, že se Sonzognem čeká dítě. Jednoho večera vpadla do bytu policie, která hledala Mina. Ale Sonzogno v momentu střelby utekl, protože si myslel, že ho Adriana zradila. Když Adriana hledala Mina, zjistila, že je zavřený. A tak požádala Astaritu o pomoc. Po výslechu s Minem ho propustili a zároveň ho zaměstnali jako informátora.

Poté se vrátil k Adrianě, ale i po tom všem se k ní choval opovržlivě a i nadále nedokázal projevit lásku.
Sonzogno se chtěl Adrianě pomstít, a tak zabil pro ni významného Astaritu. Sám Sonzogno je na útěku zastřelen.
Téhož dne spáchal sebevraždu Mino. Adriana se tuto zprávu dozvěděla z dopisu, který jí napsal ještě před smrtí.
Poprvé si uvědomuje, jakou cenu má její krása, a rozhodne se, že kvůli dítěti změní svůj život. A byla si jistá, že mu to s pomocí Minovy rodiny zaručí.
Postava #1: Adriana (od 16 let) krásná dívka, která se v průběhu románu mění z autentického až naivního děvčátka v hrdou, silnou a přesto citlivou ženu, jejíž charakter nezkřiví ani léta prostituce, přestože touží jen po klidném rodinném životě  s kupou dětí a po boku svého manžela.
Postava #2: Giacomo (Mino) (19 let) student práv, člen protifašistického odboje, hledající své umístění ve společnosti. Je protipólem Adriany: pochází z majetné rodiny, dostalo se mu dobrého vzdělání, má příležitosti, ale je znuděný, citově oploštělý a pohrdavý. Nedokáže si  odpustit, co dělal a kým byl. Až těsně před smrtí si uvědomuje, že Adrianu miloval.

### Jazyk díla
Překlad od Václava Čepa - jazyk spisovný, hovorový. Občas nespisovné výrazy (vulgarismy). Psáno v Ich - formě

### Hlavní idea díla
Profese člověka nemusí být vždy důvodem k předsudkům o jeho charakteru.

### Názor na dílo
Autor předkládá charakter postavy, která i když pracuje jako prostitutka, cítí hrdost za svou práci. Osudy postav mi připadaly vzdálené mojí realitě

### Film
1954 úspěšně zfilmována, Adrianu ztvárnila herečka Gina Lollobrigida

## Soukromé detaily

### Jazyk díla
Román je psán v ich - formě (= příběh vyprávěn v 1.os. j. č., příběh vidíme z pohledu jedné z postav.)

### Kompozice
Retrospektivně chronologicky uspořádané

Rozděleno na 2 části: 1. (1. - 9. Kapitola) - po “zajetý” život prostitutky 2. (10. - 11. Kapitola) - od seznámení s Minem

### Doba a místo děje
Itálie mezi světovými válkami, v době habešské války.
Vznik fašismu v Itálii, pochod na Řím 1922 Mussoliny.

### Směr
Neorealismus = umělecký směr, vznikl na počátku 20.stol., zahrnuje mnoho, někdy až protikladných tendencí. Nejvíce se prosadil v Itálii. Navazuje na klasický realismus.

### Děj
Tento příběh vypráví o dívce jménem Adriana, která žije se svou matkou v Římě. Adriana plní přání své matky a pracuje jako modelka u malířů, kteří malují akty. Adriana se zamiluje do Gina, ale později zjistí od Astarity, že Gino už má manželku a syna. Opustí ho a je plná smutku. Přestává pracovat jako modelka a stává se z ní prostitutka. Každý večer si vodí domů pánské návštěvy. Až jednoho dne šla se svou přítelkyní po ulicích Říma a zastavilo je auto s dvěma muži. Adriana se zamilovala na první pohled do Giacoma (Mina). Giacoma byl k ní trošku odtažitý, ale to se časem změnilo. po několika měsících ovšem zjistila, že je těhotná, ale ne s Minem, ale se Sonzognem. Sonzogno byl muž, kterého potkala při jednom setkání s Ginem. Od té doby ji pořád navštěvoval. ona se mu snažila vyhýbat poté, co se jí Sonzogno přiznal, že zabil zlatníka. adriana měla z něj strach , a proto mu dala vždy, co si přál. Postupem času se už Sonzogno neukázal. Mina jednoho dne zadržela policie, protože měl jiné názory na politickou situaci v zemi. Druhého dne ho sice pustili z vězení, ale on si nikdy nedokázal odpustit, že zradil své přátele, a proto radši spáchal sebevraždu. Adriana ho celé dny hledala, a o několik dní později dostala domů dokonce jeho dopis, ale on už byl mrtev. Adriana se rozhodla, že si její dítě zaslouží lepší život než měla ona, a proto napíše Minovým rodičům o dítěti, aby se o něj a ji postarali.

### Postavy
**Adriana** - krásná a naivní dívka z chudé rodiny

**Matka Adriany** - zklamaná životem. jako mladá dělala modelka, ale doteď dělá pouhou šičku košil. Jelikož touží po penězích, tak chce, aby se Adriana vdala za bohatého pána.

**Gino** - chudý řidič,který zatajuje, že je již ženatý, ale i přesto je snoubencem Adriany.

**Gisella** - prostitutka a jediná kamarádka Adriany.

**Giacomo (Mino)** - mladý student práv z bohaté rodiny, který se stal členem ilegální organizace zaměřené proti vládě. Měl opovrživý postoj vůči společnosti, bohatým, své rodiny i proti sobě samému. Nikdy neopětoval Adrianinu lásku, ale až těsně před svou smrtí si uvědomil, že ji miluje. Nechal jí kontakty na své vlivné přátelé a bohatou rodinu.

**Astarita** - vysoce postavený tajný policista, který Adrianu miloval a udělal by pro ni cokoliv jen aby byla šťastná…
