---
title: "Chlapec v pruhovaném pyžamu"
writer: "John Boyne"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Chlapec v pruhovaném pyžamu, John Boyne"
description: "Čtenářský deník k povinné maturitní četbě John Boyne - Chlapec v pruhovaném pyžamu"
slug: "/literatura/0004-john-boyne-chlapec-v-pruhovanem-pyzamu/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-17"
dateModified: "2021-02-17"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## John Boyne

### Národnost
Irská

### Profese
spisovatel, povídkář

### Život
Narodil se 1971 v Dublinu, kde stále žije. Vystudoval anglickou literaturu na Trinity College v Dublinu a tvůrčí psaní na Univerzitě Východní Anglie v Norwichi, kde mu byla udělena cena Curtise Browna. Nyní nabízí stipendium irským studentům na magisterském studiu na UEA.

### Další díla
vydal romány pro “dospělé” Zloděj času (2000), Srdce je neviditelná zuřivost (2017), Žebřík do nebe (2018) a romány pro “mladší čtenáře” Zůstaňte, kde jste a pak odejděte (2013), Chlapec na vrcholu hor (2015), Jméno mého bratra je Jessica (2019).
Dalším jeho dílem je román Stín mého stínu, který by měl být vydán v červenci 2020.
A také je autorem sbírky povídek Pod zemí (2015).

### Současníci
Uri Orlew, Morris Gleitzman

## Chlapec v pruhovaném pyžamu

### Žánr
román

### Vyšla
2006

### Děj
Odehrává se během druhé světové války v Berlíně. Bruno je devítiletý německý chlapec, syn nacistického velitele. Kvůli náhlému povýšení otce je Bruno se svými rodiči, 12letou sestrou Gretel a služkami nuceni přestěhovat se do Auschwitzu v Polsku.
Po přestěhování se Bruno beze sporu potýká se ztrátou svého berlínského domu a přátel. Je znuděný a osamělý. Bruno těžko přijímá fakt, že by neměl zkoumat všechna místa na kterých ještě nebyl, protože až vyroste, chce se stát průzkumníkem.
Jeho zvídavost v něm probudí každodenní pohled z okna jeho pokoje, směrem k plotu, kde se nacházejí lidé v pruhovaných pyžamech.
Ale i přes přísný zákaz matky se vydává na průzkum k drátěnému plotu, za kterým spatří sedícího chlapce jménem Šmuel.
Bruno se od něj dozví, že je Žid, a z toho důvodu je spolu s ostatními zavřen v táboře, kde musí po celý den pracovat. Rychle se stávají nejbližšími přáteli. A i když Bruno získává spoustu informací od Šmuela, tak i přesto jeho straně plotu moc nerozumí. A ani netuší, že žije vedle koncentračního tábora. Jednoho dne se opět vydá za Šmuelem, ale ten mu oznámí ztrátu otce. A proto se Bruno rozhodne, že se vplíží do tábora a společně se Šmuelem budou jeho otce hledat. Následující den přinesl Šmuel Brunovi vězeňské šaty, které vypadají jako pruhované pyžamo, aby zapadl do skupiny. V tu chvíli probíhá pochod vězňů, a tak jsou chlapci obklíčeni skupinou, která je vedena do plynové komory. Avšak Bruno si naivně myslí, že se pouze schovávají před deštěm, aby nenachladli. Přední dveře se zabouchly, všichni kolem vykřikli a nastala tma. Oba se začínají bát. Bruno vzal Šmuelovu ruku do své a pevně ji stiskl. Ale pospolu je drží jejich přátelství na život a na smrt.
Brunova matka si uvědomuje, co se stalo, když vidí hromadu oblečení před plotem. Pak už o Brunovi nikdo nikdy neslyšel.


### Postava #1
Bruno (15.4.1934) devítiletý německý chlapec, který je zvídavý a má na vše spoustu otázek, ale nikdo není schopen mu na ně odpovědět. Tak se stává naivním hlavním hrdinou.

### Postava #2
Shmuel (15.4.1934) devítiletý židovský chlapec, jehož čistá dětská duše není ušetřena hrůzám války.


### Jazyk díla
Překlad od Jarky Stuchlíkové.

### Hlavní idea díla
Konec knihy ukazuje, že když Němce a Žida stejně obléknete, nerozeznáte je.

### Názor na dílo
Pojmout takovéhle téma očima malého chlapce má obrovský potenciál. Viděla jsem před přečtením film a ten na mě ohromně zapůsobil. Mnohem víc než kniha. Většinou je kniha propracovanější, tentokrát se mi zdálo, že je to naopak. Jsou to však chlapcovy myšlenky, ty tomu dodávají kouzlo. Někdo říká, že byl Bruno trochu hloupý a nevěděl, co se kolem děje. Ale já si myslím opak. Věděl, ale nechápal. Nechápal, že mohou být ostatní tak krutí. Každopádně jak ve filmu tak i v knize jsem našla své.

### Film
2008 režisér Mark Herman.