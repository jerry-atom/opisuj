---
title: "Romeo, Julie a tma"
writer: "Jan Otčenášek"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Jan Otčenášek, Romeo, Julie a tma"
description: "Čtenářský deník k povinné maturitní četbě Jan Otčenášek - Romeo, Julie a tma"
slug: "/literatura/0018-jan-otcenesek-romeo-julie-a-tma/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Jan Otčenášek

### Národnost
česká

### Profese
účetní, úředník

### Život
Narozen 1924 v Praze. Maturoval 1943 na obchodní akademii v Praze. Krátké nucené práce v protektorátní fabrice. Zapojil se do ilegálního hnutí mládeže Předvoj. Po válce člen KSČ. Nedokončil studium na FF UK. Později pracoval jako účetní a úředník v různých funkcích. Zemřel 1979 v Praze.

### Další díla
Občan Brych (1959), Kulhavý Orfeus (1964), Když v ráji pršelo (1972)

### Současníci
Bohumil Hrabal, Ota Pavel, Arnošt Lustig

## Romeo, Julie a tma

### Žánr
novela

### Vyšla
1958

### Děj
Novela se odehrává v Praze roku 1942 během heydrichiády. Pavel se v parku zapovídá s plačící židovkou Ester, která nenastoupila do transportu do Terezína, kam už odvezli i její rodiče. Ukruje ji v pokojíku vedle krejčovské dílny jeho otce, za což hrozí poprava celé rodině. Pavel se tak ocitá v bezvýchodné situaci, zaplétá se do lží a balancuje na okraji zkázy všech. Denně Ester navštěvuje, pečuje o ní a zajišťuje ji pohodlí. Dělí se s ní o jídlo, kterého je v protektorátní Praze málo, takže ho musí i krást doma. Během krátké chvíle se do sebe zamilují a před krutou realitou utíkají do snění o budoucnosti. Dochází k atentátu na Heydricha. Otec se dozvídá o Pavlově nebezpečném tajemství. Reaguje velice chápavě
navrhuje přestěhovat Ester na venkov a Pavel za ní má přijet po maturitě. V noci před stěhováním ale Němci začnou prohledávat domy. Vychází na povrch, že o Ester už ví celý dům. Vyděšený a opilý soused Rejsek chce Ester vyhnat, ale sousedé se jí zastanou. Ester nevydrží tlak odpovědnosti za životy ostatních a uteče ven, kde ji o pár ulic dál vojáci zastřelí.

### Postava #1
Pavel je maturant prožívající první zamilovanost do židovky během heydrichiády. Je nucen porušovat ideály a čelit následkům pošetilého rozhodnutí.

### Postava #2
Ester je prosté a milé děvče židovského původu. Trpí tuší, že její rodina byla vyvražděna. Je zkušenější než Pavel a má silnější smysl pro realitu.

### Jazyk díla
Chronologické vyprávění. Er-forma. Časté střídání přímé a polopřímé řeči udává tempo ději. Časté básnické obraty a personifikace, které podtrhují Pavlovy citové a myšlenkové pochody.

### Hlavní idea díla
Zdůvodnit nesmyslnost antisemitismu a války na příkladu zamilovanosti mladých lidí.

### Názor na dílo
Autor dílo psal z pohledu adolescenta, patrně vzpomínky na své mládí. Protože jsem ale o 18 let starší než Pavel, působil na mě příliš pateticky. Vylíčení tísnivé atmosféry protektorátu je ale zdařilé. Pavel také formuluje postoj tehdejší mladé generace: “Ale co můžeš proti světu, když už jej staří tak beznadějně zpackali?”
