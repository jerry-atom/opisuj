---
title: "O myších a lidech"
writer: "John Steinbeck"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, John Steinbeck, O myších a lidech"
description: "Čtenářský deník k povinné maturitní četbě John Steinbeck - O myších a lidech"
slug: "/literatura/0015-john-steinbeck-o-mysích-a-lidech/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## John Steinbeck

### Národnost
Američan

### Profese
spisovatel

### Život
Narozen 1902 v Kalifornii. Vystudoval historii a literaturu na Stanfordu. Vystřídal mnoho zaměstnání, ze kterých pak čerpal pro svoji tvorbu. Později novinář a spisovatel. Nobelova cena za literaturu 1962 (za Hrozny hněvu). Umírá 1968 v New Yorku.

### Další díla
O myších lidech (1937), Hrozny hněvu (1939), Na východ od ráje (1968).

### Současníci
William Faulkner, Ernest Hemingway.

## O myších a lidech

### Žánr
novela, fikce, sociální realismus

### Vyšla
1937 anglicky

### Děj
Odehrává se ve 30. letech 20. století v Kalifornii v období Velké hospodářské krize a vypráví příběh dvou amerických dělníků George a Lennyho. George slíbil pečovat o mentálně zaostalého Lennyho. I když na Lennyho nadává, má ho rád a pečuje o něj jako o syna. Oba se živí jako sezónní nádenici na farmách, žijí od výplaty k výplatě s mizivou nadějí na lepší život. Na jedné farmě se oběma konečně poštěstí a svitne jim naděje na vydělání dostatku peněz, aby si mohli koupit vlastní farmu. Lenny ale v šarvátce poláme ruku předákovi Curleymu. Tím na sebe upozorní Cureyovu ženu, která je z nudy svádí dělníky a podvádí s nimi svého muže. Nabídne Lennymu, aby ji hladil po vlasech. Lenny ale neumí dobře kontrolovat svou sílu a když to začne být Curleyho ženě nepříjemné a chce se křičet, Lenny se jí snaží umlčet a nechtěně ji zlomí vaz. Uvědomí si, že provedl něco zlého a utíká místo, kde se mají s Georgem sejít, kdyby se něco pokazilo. Muži pak najdou její mrtvolu, domyslí si, co se stalo, a pod Curleyho vedením se vypraví Lennyho zlynčovat. George je nedokáže přemluvit a tak spěchá na místo, kde se skrývá Lenny. Aby kamaráda ušetřil před strašnou smrtí, milosrdně ho zastřelí do zátylku.


### Postava #1
*George Milton* je obyčejný muž, je chytrý a dobrotivý.

### Postava #2
*Lenny Small* je mentálně zaostalý, velice silný, ale neškodný jako malé děcko.

### Jazyk díla
Vypravěč na začátku každé ze šesti kapitol popisuje spisovným jazykem osoby a místa, především přírodních scenérie, pak předává slovo dialogu postav formou přímé řeči, do které už vstupuje jen minimálně. Postavy spolu hovoří jazykem příslušícím jejich společenskému původu.

### Hlavní idea díla
Autor pomocí dojímavého příběhu poukazuje na tíživé poměry generace stižené velkou krizí.

### Názor na dílo
Líbil se mi jednoduchý a chronologický děj, jasně vyjádřené vztahy mezi lidmi. Autor barvitě popisuje přírodu.
