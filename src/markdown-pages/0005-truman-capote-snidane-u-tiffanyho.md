---
title: "Snídaně u Tiffanyho"
writer: "Truman Capote"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Snídaně u Tiffanyho, Truman Capote"
description: "Čtenářský deník k povinné maturitní četbě Truman Capote - Snídaně u Tiffanyho"
slug: "/literatura/0005-truman-capote-snidane-u-tiffanyho/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-20"
dateModified: "2021-02-20"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Truman Capote

### Národnost
americká

### Profese
prozaik, esejista, autor krátkých - mistrně napsaných textů

### Život
Narodil se 1924 v New Orleansu v Lousianě. Jako dysgrafik se naučil sám už v pěti letech číst a psát. V osmi letech už předčítal svoje vlastní příběhy. Už v jedenácti letech se stal takříkajíc závislým na psaní. Roku 1935 nastoupil do školy. Poté pokračoval na vojenské akademii. Roku 1939 se přestěhoval do Greenwiche, kde psal do školních novin a časopisu. V roce 1942 složil závěrečné zkoušky v New Yorku na soukromé škole v Upper West Side. Dodnes je tam každoročně jeho jménem udílena literární cena. Jako alkoholik, drogově závislý homosexuál zemřel v roce 1984 v Los Angeles.

### Další díla
první dílo Jiné hlasy, jiné pokoje (1948), sbírka povídek Strom noci (1949), novela Luční harfa (1951)

### Současníci
William Styron, Flandery O'Connorová

## Snídaně u Tiffanyho

### Žánr
novela

### Vyšla
1958 / Název originálu: “Breakfast at Tiffany´s”

### Přeloženo
Jarmila Fastrová

### Děj
Příběh je o Holly Golightlyové, která neměla nijak lehké dětství. Když byla malá, tak ji zemřeli rodiče a ona zůstala sama se svým bratrem Fredem. Oba byli u pěstounské rodiny, odkud společně utečou. Při útěku narazili na koňského lékaře (veterináře), který se stal Hollyiným manželem, i když jí bylo teprve 14 let. A tak bratr žil společně s nimi. Poté co ho povolali na vojnu, Holly utekla do New Yorku, kde se začala stýkat s podivnými muži a pohybovat ve vyšší společnosti.
Vypravěč se s Holly setkává ve chvíli, kdy mu vleze oknem do bytu, aby unikla před horlivým a rozdováděným mužem, který ji chtěl pokousat. Vypravěč připomíná Holly jejího bratra Freda, a tak ho mu začne říkat Fred. Stanou se z nich dobří přátelé a vypravěč je stržen do víru Hollyina vzrušujícího (ač prostého) života. Ve svém nitru oba touží po “štěstí” a pocitu sounáležitosti, což jsou sny, které se v očích lidí dost mladých na to, aby mohli doufat, podobají osudu. Do jejich životů se ale vkrádají náznaky stínů. Hollyinu rodinu postihne tragédie, která nakonec změní její vztah k vypravěči, a její nevinnost je vystavena těžké zkoušce, když ji pravidelný zákazník, mafián Salvatore “Sally” Tomato, zneužije nejen sexuálně. Ukázalo se, že ten řídil své obchody venku pomocí vzkazů přes Holly. Každý čtvrtek ho chodí do věznice navštěvovat. Za to dostává každý týden 100 dolarů od jeho advokáta. A to se stává i důvodem jejího zatčení, protože předává z vězení dešifrované zprávy. V té době byl jejím partnerem José Ybarra-Jaegar, se kterým společně bydleli. Po této informaci se od ní odstěhoval, aby nepošpinila jeho pověst. Holly tedy odjela sama do Ria de Janeira. Dokonce i bez kocoura, ale o toho se nakonec postaral sám vypravěč. Do New Yorku se už nikdy nevrátila.
Později vypravěči dorazil od Holly korespondenční lístek, a ten se stal prvním a zároveň i posledním.

### Postava #1
Holly Golightlyová (19 let, za svobodna - Lulamae Barnesová) je sexuálně svobodná, hédonistická prostitutka. Žije pro kouzlo okamžiku, ignoruje důsledky svých činů a morální zásady si vytváří za pochodu. Je stejně nespoutaná a nezkrotná jako její bezejmenná kočka.

### Postava #2
Vypravěč - nejmenovaný spisovatel (asi autor), přichází do New Yorku splnit si svůj sen - vybudovat si kariéru spisovatele.
Jazyk díla: spisovný místy hovorový, místy vulgarismy, francouzská slova v řeči Holly, anglické názvy míst a osob

### Hlavní idea díla
Studie složité a rozporuplné osobnosti mladé dívky, která hledá své štěstí a místo ve světě. (Pokud najde na světě místo, na kterém se bude cítit jako U Tiffanyho, tak se usadí, vybalí si kufry a dá kocourovi jméno.)

### Názor na dílo
Krátký rozsah i téma může svádět k povrchnímu závěru, že novela postrádá hloubku. Ale příběh je ořezán na nezbytné minimum, kde vše má svůj význam. S postavami jsem se neztotožňovala.

### Film
1961, režisér Blake Edwardsen. Film se odchyluje od předlohy. Např. Ve filmu se vypravěč jmenuje Paul Varjak, ale v knize nemá jméno. Na konci filmu se hrdina a Holly do sebe zamilují a zůstanou spolu, zatímco v novele neexistuje žádná milostná aféra.
