---
title: "Krysař"
writer: "Viktor Dyk"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Viktor Dyk, Krysař"
description: "Čtenářský deník k povinné maturitní četbě Viktor Dyk - Krysař"
slug: "/literatura/0016-viktor-dyk-krysar/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Viktor Dyk

### Národnost
česká

### Profese
básník, prozaik, dramatik

### Život
Narozen 1877 u Mělníka. Absolvoval gymnázium a vystudoval práva v Praze. Právu se ale nikdy profesně nevěnoval. Od mládí se zapojoval do odboje proti monarchii, řazen mezi anarchistické buřiče. Později inklinoval k nacionalismu. Zemřel na mrtvici 1931 na území dnešního Chorvatska.
Další díla: Poezie: Marnost, Milá sedmi loupežníků, Devátá vlna. Próza: Můj přítel Čehona, Prsty Habakukovy. Drama: Zmoudření Dona Quijota.
Současníci: Stanislav Kostka Neumann, František Gellner, Karel Toman.

## Krysař

### Žánr
novela

### Vyšla
1915

### Děj
Krysař přichází do města Hameln, seznamuje se s milenkou Agnes a domlouvá si s radními, že vyžene z města krysy za sto rýnských. Během svého práce ve městě se seznamuje s jeho obyvateli a prohlédne jejich malost, hrabivost a podlost. Ke splnění svého úkolu používá svou píšťalu, na kterou když slabounce hraje, krysy ho následují až do řeky, kde utonou. Radní pak odmítají proplatit mzdu, neboť se jim zdá příliš vysoká za tak málo námahy. Argumentují tím, že smlouva nemá patřičné náležitosti. Krysař zuří, slibuje radním mstu. Později v v Agnesině náruči se nachází mír. Její druhý milenec ji ale ublíží a zlomená Agnes spáchá sebevraždu skokem do propasti. To uvolní bolest a zášť krysaře, zahraje mocně na píšťalu a všichni lidé ho následují jako krysy k propasti, kam svrhne je a nakonec i sebe. Přežije jen ti, co nerozumí nebo nemohou chodit: novorozeně a rybář Jorge.

### Postava #1
*Krysař* nemá vlastní jméno. Obdařen výjimečnou schopností ovládat mysl hrou na píšťalu. Je si vědom své destruktivní moci, ale nikdy ji nepoužil proti lidem. Procestoval celý svět, je samotářský, citlivý, moudrý, má vysoké morální hodnoty, vyzná se v lidech a udržuje se proto odstup od všech kromě Agnes.

### Postava #2
*Agnes* je mladá, hezká a citlivá milenka Kristiána. Zamiluje se do krysaře. Jakmile zjistí, že čeká s Kristiánem dítě, volí raději smrt než život v hanbě.

### Jazyk díla
Využívá všech prostředků k navození dojmu starého textu: archaismy (býti), historismy (konšelé), postfixy (kráčelť). Dějem provází vypravěč s využitím přímé a nepřímé řeči. Rozděleno na 25 krátkých kapitol, které se postupně ozřejmují vztahy a děj.

### Hlavní idea díla
Nejsem schopna jednoznačně interpretovat. Tuším, že krysařova postava by mohla být projekcí autora a děj reprezentuje jeho vztah ke společnosti.

### Názor na dílo
Zaujala mě hluboká symbolika díla. Autor použil legendu o krysařovi pro její nadčasovost. Stejně tak se odkazuje na nadčasového Fausta v kapitole, kde krysaře svádí magistr Faustus z Wittenberka. Dokonce se krysaři přiznává, že mu slouží sám ďábel a proměňuje starou ženu v Helenu z Tróje. Možná lze i Agnesinu sebevraždu chápat jako podobu s Faustvou Marketou. Možná by se dalo říci, že naše srdce nám brání v poznání. Fascinující je, že krysař zneužil lidskou touhu po utopickém životě k tomu, aby je svrhl z propasti. Původní německá legenda se zakládá na skutečných událostech města Hameln.

### Obsah kapitol
1. Představení krysaře, jeho píšťaly a jeho práce (údělu). Umístění do prostoru v městě Hameln. Krysař navazuje milenecký vztah s Agnes.
2. Popis klidného domu Agnes a práce krysaře.
3. Představení Jorge Seppa, zdravého a pohledného rybáře, který je ale chudý a zpomalený. Jorge je vysmíván obyvateli Hamelnu, akceptuje svou pozici nuzáka, nedoufá v zlepšení.
4. Představení Agnesina milence Kristiána a jeho prostopášného strýce Ondřeje. Kristián je pro Agnes dobrá partie. Doufá v dědictví po strýci a obává se, že strýc zplodí potomka, který ho o dědictví připraví.
5. Popis nedalekého kopce Koppel a přilehlé hluboké propasti.
6. Jorge Sepp spí u své chýše. Přijdou k němu půvabné a mladé dívky: Lora, dcera řezbáře Wolframa, a Katchen, dcera pekaře Grilla. Představují si, že Jorge by stál za hřích. Ale jakmile muž procitne, uvědomí si, že Jorge není dobrá partie, vysmějí se mu a utečou.
7. Jorge je tolik ponížen výsměchem Lory a Katchen, že uškrtí svého milovaného drozda a uprostřed noci klečí před domem, kde obě dívky žijí. Potká jej krysař a varuje ho před jeho počínáním. Jorge je ale pomalý a nereaguje.
8. Hospoda “U žíznivého člověka” krčmáře Konráda Rogera je ústředním společenským bodem celého Hamelnu. Popíjí tu konšelé truhlář Gottlieb Frosch a soukeník Bonifác Strumm. Přichází k nim krysař a veřejně se domáhá své doměny sta rýnských za provedenou práci, zbavení mesta krys. Oba konšelé odmítají odměnu vyplatit se dvěma argumenty. Zaprvé krysař není legitimní občan a zpochybňují, zda ten, co žádá odměnu, je ten samý, kdo uzavřel smlouvu. Za druhé smlouva neobsahuje dle zvyklostí větu: “K tomu mi dopomáhej bůh,” čímž ji činí neplatnou. Krysař slibuje boj.
9. Krysař rozmýšlí strašlivou pomstu. Pro Agnes povolí a uklidní se.
10. Gottliebu Froschovi a Bonifáci Strummi se zdají zlé sny. Tíží je svědomí.
11. Krysař rozjímá. Zdá se být klidný.
12. Ráno po noci, kterou krysař a Agnes strávili spolu, přistihne Kristián krysaře v agnesině zahradě. Zuří. Ale nezaútočí.
13. Krysař jde na procházku za město a zdá se, že jeho schopnosti se zesilují. Na konci potká Jorga, usnul v lodi unášené řekou.
14. Krysař se vrací z procházky do města. Mám pocit, že jde do pasti.
15. Krysař se rozhodne znovu jít do krčmy.
16. V krčmě je pusto, lidi odehnala přítomnost podezřelého cizince, Faustus z Wittenberku. Cizinec krysaři prozradí, že slouží Jemu (někdo nahoře, asi ďábel), umí cestovat neomezenou rychlostí a proměňovat věci. A že On může krysaře učinit ještě mocnějším. Pak své umění demonstruje na třech příkladech, z nichž ale vyplývá, jako by cizinec spíš trpěl bludy, krysař žádné proměny nevidí. Cizinec mu ale na konci prozradí, že to jeho srdce mu překáží v prozření. Pak zmizel jakoby kouzlem.
17. Krysař navštíví Agnes, ta je však nějaká pobledlá a něco tají.
18. Agnes prozradí krysaři, že ji Kristián znásilnil a oplodnil.
19. Agnes vyprosí u matky opakování pohádky O Sedmihradsku. V pohádce se praví, že cesta do bájného údolí střeží sedm hradů a cesta tam vede propastí.
20. Personifikace řeky. Utonutím v řece lze si zkrátit utrpení.
21. Matka Agnes ji hledá. Ptá se i Kristiána, ten neví. Pak přijde o rozum.
22. Krysař nachází Agnesinu matku šílenou a dozví se od ní, že Agnes odešla spáchat sebevraždu.
23. Krysař v zoufalství a v touze po pomstě za smrt své milenky začal mocně hrát na píšťalu a lidé města Hameln jej následovali.
24. Krysař zavedl lidi k propasti a lidé do ní popadali. Pak v ní skoncoval se životem i krysař.
25. Jediný, kdo přežil, byl rybář Jorgen Sepp. Opožděně, ale přece i on byl zlákán hudbou. Cestou našel plačící dítě. A na kraji propasti pláč dítěte překonal jeho touhu vstoupit do sedmihradska.
