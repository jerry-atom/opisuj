---
title: "Smrt krásných srnců"
writer: "Ota Pavel"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Smrt krásných srnců, Ota Pavel"
description: "Zápis ze čtenářského deníku k povinné maturitní četbě knihy Smrt krásných srnců od autora Ota Pavel"
slug: "/literatura/0001-ota-pavel-smrt-krasnych-srncu/"
authorName: "@sari"
dateCreated: "2020-06-14"
dateModified: "2020-06-14"
dateMublished: "2020-06-14"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Ota Pavel

### Národnost
česká

### Profese
prozaik, novinář, reportér

### Život
Narozen 1930 v Praze jako Otto Popper, otec Leo židovského původu, matka Hermína katolička. 1943 byli bratři Hugo a Jiří, o rok později i otec, deportováni do koncentračních táborů. Přežili. V dospělosti sportovním reportérem. Později vážné psychické onemocnění. Umírá 1973 na srdeční selhání.

### Další díla
Jak jsem potkal ryby (1974), Dukla mezi mrakodrapy (1964), Plná bedna šampaňského (1967)

### Současníci
Ivan Klíma, Bohumil Hrabal, Arnošt Lustig

## Smrt krásných srnců

### Žánr
soubor povídek

### Vyšla
1971

### Děj
Kniha sestává z 9 povídek:
*Nejdražší ve střední Evropě* - Předválečné období, rodina žije v Praze. Tatínek naletěl při koupi rybníku ve víře, že je plný kaprů. Při výlovu ale zjistil, že kapr byl v rybníku jen jeden. Prodejci (doktor Václavík) se pomstil o pár let později tak, že mu prodal nefunkční ledničku za stejné peníze.

*V službách Švédska* - Tatínek se stal šampionem mezi obchodními cestujícími firmy Elektrolux a zakoukal se ředitelovy (František Korálek) ženy Irmy. Aby se jí zalíbil, spřátelil se s prestižním malířem (Nechleba), u kterého chtěl vyprosil namalování portrétu Irmy. Úsilí nebylo korunováno úspěchem.

*Smrt krásných srnců* - Po vypuknutí války rodina utíká na venkov (Buštěhrad). Bratři Jiří a Hugo mají nastoupit do koncentračního tábora. Tatínek proto riskuje zastřelení německými vojáky a s pomocí převozníka Proška a jeho psa Holana uloví kapitálního srnce, jehož masem vykrmí své syny před odjezdem. Jiří se z Mauthausenu vrátil s váhou 40.5 kg a přiznává, že srnec mu možná zachránil život. Povídka se stala ústředním motivem stejnojmenného filmu Karla Kachyňky z r. 1986.

*Kapři pro Wehrmacht* - Tatínek před válkou vlastnil rybník, který mu Němci zabavili. Když byl před vánoci povolán do Terezína, s pomocí Oty přes noc vylovili rybník. Oto pak kapry směnil za jídlo, což pomohlo jemu a matce v nepřítomnosti otce.

*Jak jsme se střetli s Vlky* - Poválečná vzpomínka na to, jak tatínek prohrál závod o ulovení největší štiky s rodinou Vlkových.

*Otázka hmyzu vyřešena* - Otec se po návratu z koncentračního tábora snaží navázat na své předválečné úspěchy obchodního cestujícího. Zdálo se, že se na něj usmálo štěstí v prodeji nového druhu mucholapek. Prvotní úspěch ale vystřídal debakl, protože mouchy nehynou přilepeny na mucholapku, nýbrž odlétají chcípnout nevidno kam.

*Prase nebude!* - Tatínek se nadchnul pro chov prasat s příslibem, že jedno pak dostane za odměnu. JZD ale původní ústní dohodu zrušilo a místo odměny mu naúčtovali doplatit ztráty. Tato povídka byla během komunismu cenzurována.

*Běh Prahou* - Vzpomínka na volby v květnu 1946, které poprvé a naposled demokraticky vyhrála KSČ. Na jejich podporu tatínek uspořádal agitační závod svých synů s volebními čísly na prsou. Oto s volebním číslem KSČ vběhl do davu demonstrující opozice, kde ho surově zbili. Otec později vystřízliví z naivního očekávání od komunismu.

*Králíci s moudrýma očima* - Na sklonku života se tatínek nadchl pro pěstování králíků. Chce získat ocenění na výstavě a pak králíky dobře prodat. Na výstavě ale není oceněn a zklamaně králíky vypouští na pole. Domů se vrací pěšky bez peněz a ráno umírá na srdeční příhodu.

### Postava #1
*Leo* - Otův otec. Bystrým, nezlomný, sukničkář, idealista. V mládí nasazen v bojích v Africe.

### Postava #2
*Hermína* - Otova matka. Kniha je jí věnována, ačkoliv pozornost strhává spíše otec. Klidná, pečující, obětavá, tolerantní k manželovým přešlapům.

### Jazyk díla
Povídky řazeny chronologicky. Autor vypráví své vzpomínky na otce. Hovorový jazyk s řadou slangových výrazů. Kladen důraz na ironii a kontrast exotických postav a událostí.

### Hlavní idea díla
Domnívám se, že Ota Pavel knihu psal především pro svoji matku, jako jakési album vzpomínek na zesnulého manžela.

### Názor na dílo
Léta psaní sportovních reportáži nejspíš Otu Pavla naučila zkrátit příběh do malých a fascinujících obrazů s ostrou pointou. V povídkách postupně mizí důvod k optimismu a je cítit narustající tíže, jak život dává a bere.
