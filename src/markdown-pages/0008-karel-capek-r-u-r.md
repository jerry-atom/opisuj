---
title: "R.U.R."
writer: "Karel Čapek"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, R.U.R., Karel Čapek"
description: "Čtenářský deník k povinné maturitní četbě Karel Čapek - R.U.R."
slug: "/literatura/0008-karel-capek-r-u-r/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-20"
dateModified: "2021-02-20"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Karel Čapek

### Národnost
česká

### Profese
spisovatel, novinář, dramatik

### Život
Narozen 1890 (Malé Svatoňovice). Vystudoval gymnázium v Brně (ul. kap. Jaroše). Získal doktorát z filozofie na Univ. Karlově. Od 21 let Bechtěrevova nemoc, takže nenarukoval do 1.s.v. Po studiu krátce učitelem, později novinářem, než odešel na politický protest. Dramatik ve Vinohradském divadle. Pořádal schůzky s významnými lidmi (T.G.M., E. Beneš, F. Peroutka) které se jmenují Pátečníci. Anšlus nesl těžce. Zemřel 1938 na zápal plic.

### Další díla
Próza: Trapné povídky, Továrna na absolutno, Krakatit, Zahradníkův rok. Dramata: Loupežník, Věc Makropulos, Bílá nemoc. Ostatní: Dášeňka, čili život štěněte, Hovory s T. G. Masarykem.

### Současníci
Boris Pasternak, Vítěslav Nezval

## R.U.R.

### Žánr
drama

### Vyšla
1920

### Děj
Předehra začíná na nejmenovaném ostrově v neznámé budoucnosti. Celý ostrov slouží výhradně jako střežená továrna na roboty: biologické stroje sestavované na výrobních linkách podle původního Rossumova receptu, převedeného do průmyslového měřítka jeho synem. Roboti jsou na pohled k nerozeznání od lidí, ale jsou inteligenější, silnější a odolnější, nemohou se rozmnožovat, nemají city a svobodnou vůli, jednají jen jako automaty. Na ostrov přijíždí krásná slečna Helena Gloryová, aby se pokusila roboty zrovnoprávnit. Zde poznává ředitele Harryho Domina, Ing. Fabryho, Dr. Galla, Dr. Hellmaiera, kozula Bushmana a stavitele Alquista. Všichni se do ní zamilují, ale ona se provdá za Domina. První dějství se odehrává o 10 let později. Lidstvo se stalo na práci robotů fatálně závislé a dokonce je zneužívají jako vojáky. Heleně se nepodařilo roboty přimět k touze po svobodě a tak přesvědčila Dr. Galla, aby se odchýlil od původního receptu a zkusil robotům vyrobit duši, svobodnou vůli. Tito roboti se dostali do světa, začali revoluci a vyhlásili lidem vyhlazovací válku. V druhém dějství roboti obléhají Dominův dům a chtějí recept na jejich výrobu. Ten ale Helena v rozčílení spálila. Nakonec roboti zabijí všechny, kromě Alquista. Ve třetím dějství je Alquist posledním člověkem na světě. Je roboty nucen k znovuobjevení výroby, ale neúspěšně. Nakonec rozpoznává lásku a lidskost mezi roboty Primusem a Helenou. Oba posílá do světa, aby žili jako Adam a Eva z bible.

### Postava #1
Harry Domin - bezohledný idealista, který chce za každou cenu osvobodit lidstvo tím, že ho zbaví dřiny

### Postava #2
Alquist - protipólem Domina, věří v náboženské ideály, když je mu úzko, pracuje jako dělník, což mu pak zachrání život
Jazyk díla: Předehra komediální, 3 dějství tragická.

### Hlavní idea díla
Varování před bezohlednou honbou za ideály a zneužívání vynálezů k válečným účelům.

### Názor na dílo
Jednání postav se mi zdálo příliš naivní. V závěru je opomenuto, že roboti Helena a Primus nemohou být jako Adam a Eva, protože nemohou mít potomky.
