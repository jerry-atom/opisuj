---
title: "Lakomec"
writer: "Molière"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Molière, Lakomec"
description: "Čtenářský deník k povinné maturitní četbě Molière - Lakomec"
slug: "/literatura/0009-moliere-lakomec/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-26"
dateModified: "2021-02-26"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Molière

### Národnost
francouz

### Profese
dramatik, herec, básník

### Život
Narozen 1622 v Paříži, vlastním jménem Jean-Baptiste Poquelin. Získal doktorát z práv. Proti vůli rodiny ale utekl k divadlu, proto používal pseudonym Molière. Přechodné období neúspěchu ukončila přízeň krále Ludvíka XIV. Zemřel na tuberkulózu 1673.

### Další díla
Tartuffe, Don Juan, Misantrop, Zdravý nemocný.

### Současníci
Jonathan Swift, Daniel Dafoe, Jeane Racin

## Lakomec

### Žánr
drama, komedie

### Vyšla
1668

### Děj
**Jednání 1.**
Harpagon v zapomnění vyřkne nahlas, kde má skrytu svou truhlici peněz. Obává se, že to zaslechli dcera Elisa a syn Kleante. Ti mají ale jiné starosti. Chtějí mu říct, že si vybrali nevěstu a ženicha. Harpagon je to ale nenechá doříct a oznámí jim, že pro sebe si vybral za nevěstu Marianu a Elise bohatého vdovce Anselma. Přichází Valere, Elisin milenec a uklidňuje ji, že otcovo rozhodnutí nějak zvrátí.

**Jednání 2.**
Pro Kleanta je to rána pod pás, protože miluje Marianu. Své city ale zatají, aby mohl finančně pomoci Marianě i její matce. Tyto peníze si chce půjčit od lichváře, s nímž ale jedná pomocí prostředníků, takže do poslední chvíle netuší, že je to jeho otec. Jakmile se to Harpagon dozví, rozzlobí se na syna za jeho rozmařilost a vyžene ho.

**Jednání 3.**
Mariana přijde na námluvy. Když poprvé spatří Harpagona, je zděšena jeho vzhledem a šokována tím, že Kleante je jeho syn. Během večeře si Kleante utahuje z otce, flirtuje s Marianou a daruje jí otcův briliantový prsten. Nakonec Harpagon musí odejít kvůli obchodním záležitostem.

**Jednání 4.**
Harpagon má podezření, že Kleante miluje Marianu a lstí to ze syna vyláká. To vede k hádce, kterou se snaží urovnat Jakub tím, že každému řekne, že ten druhý ustupuje. Syn a otec jsou chvíli na sebe milí, dokud si neuvědomí Jakubovu lest. Harpagon nakonec vyžene Kleanta s výhružkou, že ho vydědí. La Flèche (Šipka), Harpagonův sluha mezitím ukradne truhlici a předá ji Kleantovi.

**Jednání 5.**
Anselm nechce nutit Elisu k nedobrovolnému manželství. Jakub podezírá Valera z ukradené truhlice. Valere ale o krádeži nic neví a myslí si, že tím pokladem je Elisa. To Harpagona zmate a zlobí se na Elisu, jak si mohla něco začít se zlodějem. Valere musí přiznat, že je synem bohatého šlechtice, že jeho rodina téměř zemřela při potopení lodi a že tam zachránil a poznal Elisu, se kterou se zamilovali a tak pracuje jako správce domu, aby jí byl poblíž. Uvědomí si, že Mariana je Valerova sestra a Anselmo je jejich otec. Harpagon se ale spíš zajímá o svou ukradenou truhlici. Kleante nakonec vyjedná s Harpagonem svatbu s Marianou a vrací mu truhlici.

### Postava #1
Harpagon - šedesátník, lakomý, podezřívavý až paranoidní, tyranizuje své služebnictvo i rodinu svou lakotou.

### Postava #2
Kleante - syn Harpagona, rozmařilý mladenec vytříbených mravů, miluje Marianu.

### Jazyk díla
Jedna scéna v Paříží v Harpagonově domě. Drama bez vypravěče. Chronologický postup. Hovorový jazyk, který v dnešním kontextu už zní zastarale.

### Hlavní idea díla
Pobavení diváků nadčasovými mezilidskými sváry. Možná i kritika tehdy běžné praxe - majetkových sňatků.

### Názor na dílo
Překvapivě složitá hra, kde je těžké vyznat se v postavách a prohlédnout jejich motivy. Hra byla údajně pokroková a tak jí dlouho trvalo, než si získala svůj věhlas. Molière se inspiroval latinskou divadelní hrou Aulularia od římského dramatika Tituse Macciuse Plautuse přibližně z roku 195 př.n.l., česky se hra nazývá Komedie o hrnci.
