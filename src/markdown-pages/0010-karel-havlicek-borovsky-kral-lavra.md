---
title: "Král Lávra"
writer: "Karel Havlíček Borovský"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Karel Havlíček Borovský, Král Lávra"
description: "Čtenářský deník k povinné maturitní četbě Karel Havlíček Borovský - Král Lávra"
slug: "/literatura/0010-karel-havlicek-borovsky-kral-lavra/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-26"
dateModified: "2021-02-26"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Karel Havlíček Borovský

### Národnost
česká

### Profese
novinář, básník, učitel

### Život
Narodil se 1821 na Vysočině v Havlíčkově Borové. Na gymnáziu se nadchl pro obrozenecké a panslovanské ideje. Studia dotáhl až na kněžský seminář, kde ale nebyl spokojený a za kritiku římskokatolické církve ho vyloučili. Od panslovanských idejí upustil při následném pokusu stát se učitelem v Rusku. Po návratu do Prahy se stává velmi oblíbeným novinářem a kritikem. Do 1848 vedl Pražské noviny a Národní noviny. Pozdeji vydával časopis Slovan, dokud mu to nebylo zakázáno. Za kritiku monarchie odsouzen k vyhnanství v Tyrolském Brixenu (1851-1854). Po návratu umírá na tuberkulózu v r. 1856.

### Další díla
Epigramy, Křest sv. Vladimíra, Král Lávra, Tyrolské elegie, Obrazy z Rus, Epištoly kutnohorské, 

### Současníci
Karel Jaromír Erben, Josef Kajetán Tyl, Božena Němcová, Karel Hynek Mácha

## Král Lávra

### Žánr
satirická báseň, 37 strof po 7 verších, zpěv

### Vydáno
1854

### Děj
Král Lávra byl irský král, který své zemi vládne dobře a zbytečně nesužuje své poddané. “Jen jednu slabost ten král dobrý měl, že jest na holiče tuze zanevřel.” Král nosil dlouhé vlasy, které si nechal stříhat jen jednou do roka. Holiče pak ale okamžitě nechal popravit. Jeden rok padl los na Kukulína. Jeho matka si vyprosila milost pro syna. Králi ho povýšil na svého doživotního holiče, ovšem nesmí nikdy vyslovit pravdu, že Lávra pod vlasy skrývá oslí uši. Kukulína však tajemství trápilo. Na radu poustevníka vyzradil své tajemství staré vrbě. Později šli kolem muzikanti a pan Červíček si z proutku té vrby vyrobil náhradní kolík ke své base. Když na tu basu zahrál na královském dvoře, basa všem poddaným vyzradila, že: “Král Lávra má dlouhé oslí uši: král je ušatec!” Lávra se nikdy nedozvěděl pravdu, lid si na jeho uši zvykl a všem se pak žilo dobře i bez poprav.

### Postava #1
**Král Lávra** - tají, že má oslí uši (symbol hlouposti). Nechává se holit jen jednou za rok, aby zmenšil počet poprav. Po zrušení poprav skutečně dojde k prozrazení.

### Postava #2
**Kukulín** - vzor prostého člověka, který sice jedná čestně, ale nic nezmůže proti své přirozenosti.

### Jazyk díla
Vypravěč provádí dějem a uvádí přímou řeč. Vyskytují se pohádkové motivy.

### Hlavní idea díla
Autor ji píše během svého vyhnanství v Brixenu. Za vzor mu posloužil starý mýtus o bájném antickém králi Midasovi, který měl oslí uši a některé irské legendy. V ději skrývá kritiku monarchie.

### Názor na dílo
Pro pohádkové vyznění a bez znalosti dobového kontextu a osobnosti autora by nebylo možné báseň správně interpretovat. Jako kritika mi přijde velmi mírná a optimistická, protože konec příběhu je dobrý. Jako by autor spíš jen poukazoval na zbytečný strach monarchie před liberárnějšími postoji.
