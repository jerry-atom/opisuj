---
title: "Ostře sledované vlaky"
writer: "Bohumil Hrabal"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Bohumil Hrabal, Ostře sledované vlaky"
description: "Čtenářský deník k povinné maturitní četbě Bohumil Hrabal - Ostře sledované vlaky"
slug: "/literatura/0017-bohumil-hrabal-ostre-sledovane-vlaky/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Bohumil Hrabal

### Národnost
česká

### Profese
spisovatel, prozaik

### Život
Narodil se 1914 v Brně - Židenicích svobodné matce, která pracovala jako pomocná účetní v pivovaře, kde se seznámila a později i provdala za hlavního účetního Františka Hrabala. Po maturitě studoval na právnické fakultě UK v Praze a navštěvoval přednášky z dějin literatury, umění a filozofie. Vinou uzavření VŠ za okupace studia dokončil až v roce 1946. Během 2. světové války pracoval jako železniční dělník a výpravčí, později jako pojišťovací agent, obchodní cestující. Do svého úrazu pak nesměl publikovat, uveřejňoval proto svá díla v samizdatu a exilu. Zaměřil se spíše na prózu.Od 60.let byl spisovatelem z povolání.  Zemřel 1997 v Praze po pádu z okna Ortopedické kliniky na Bulovce, kde se léčil.

### Další díla
Perlička na dně (1963), Pábitelé (1964), Postřižiny, Obsluhoval jsem anglického krále

### Současníci
Vladimír Páral, Ota Pavel

## Ostře sledované vlaky

### Žánr
tragikomická novela (vznikla přepracováním starších rukopisů)

### Vyšla
1965

### Děj
Děj novely se odehrává v poslední válečné zimě na malé železniční stanici. Hlavním hrdinou je mladý a nesmělý Miloš Hrma, který se zde zaškoluje na výpravčího. Je zamilován do konduktérky Máši, ale když má dojít k naplnění jejich milostného vztahu, selže. Pokusí se o sebevraždu podřezáním žil, ale na poslední chvíli je zachráněn. Miloš se dozvídá, že výpravčí Hubička otiskl při noční službě telegrafistce Zdeničce na zadnici všechna staniční razítka, je z toho velké vyšetřování a výpravčího čeká disciplinární řízení. Hubička oznámí Milošovim že během příští noci projede stanicí ostře sledovaný německý vojenský transport se zbraněmi. Dohodnou se, že vlak vyhodí do vzduchu. V rozhodující den se Miloš opět setkává s Mášou a domluví si novou schůzku. Na stanici přinesla večer balíček s výbušninou krásná Viktoria z odboje, s níž získává Miloš svou první milostnou zkušenost. Cítí se plný síly, a tak klidně očekává blížící se transport. Když vlak přijíždí, vyleze Miloš na semafor a hodí bombu doprostřed čtrnáctého vagonu a sleduje, co se bude dít. Ale německý voják v posledním vagonu si ho všimne a vystřelí. Ve stejné chvíli vystřelí i Miloš a oba skončí v příkopu vedle trati. Němec má těžce poraněné břicho a hodně trpí, takže ho Miloš musí zastřelit. Ještě slyší výbuch vlaku, bere Němce za ruku a umírá.

### Postava #1
*Miloš Hrma* (22 let) - nesmělý, zakomplexovaný mladík se zaškoluje na výpravčího na malé železniční stanici v Polabí, v kraji, kde autor vyrůstal.

### Postava #2
*Výpravčí Hubička* - zkušený výpravčí a záletník, který se proslaví aférkou s orazítkovaným zadečkem telegrafistky Zdeničky.

### Postup
chronologický

### Odehrává se
Druhá světová válka - únorové dny 1945 - malé nádraží v Kostomlatech.

### Jazyk díla
Ich forma. Spisovná čeština, hovorová s vulgarismy v přímé řeči. Německé věty, složitá souvětí. Sled jednotlivých událostí je přerušován krátkými retrospekcemi (návraty do minulosti) hlavního hrdiny. Typický styl pro Hrabalova díla.

### Hlavní idea díla
Panicové historii nepíší, aneb kde by byli muži bez žen.

### Názor na dílo
Příběh mě nezaujal svou tématikou ani vyzněním. Zápletka je banální a fascinace erotikou samoúčelná až obscénní.

### Film
1966 - byl oceněn Oscarem za nejlepší cizojazyčný film v roce 1967, režie: Jiří Menzel.

### Pojmy s maturitě
* *SAMIZDAT* = způsob, jakým občanští aktivisté obcházejí cenzuru v represivních režimech, zejména v zemích východního bloku v době studené války. Jde o vydávání novinového obsahu vlastním nákladem v ilegalitě.
* *PRÓZA* = protiklad vznešeného verše (logické věty, gramatická pravidla, text do odstavců, neodděluje se pauzou)
* *EPIKA* =  má děj, vypráví příběh
* *NOVELA* = kratší útvar, menší množství postav
* *RETROSPEKCE* = návraty do minulosti


