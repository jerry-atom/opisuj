---
title: "Spalovač mrtvol"
writer: "Ladislav Fuks"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Ladislav Fuks, Spalovač mrtvol"
description: "Čtenářský deník k povinné maturitní četbě Ladislav Fuks - Spalovač mrtvol"
slug: "/literatura/0019-ladislav-fuks-spalovac-mrtvol/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Ladislav Fuks

### Národnost
Československá

### Profese
spisovatel

### Život
Narodil se 1923 v Praze a zemřel 1994 tamtéž. Vystudoval filozofii na Karlově univerzitě, začínal jako úředník ve státním památkovém ústavu, později se plně věnoval spisovatelské činnosti.

### Další díla
Pan Theodor Mundstock (1963), Mí černovlasí bratři (1964), Myši Natálie Mooshabrové (1970), Vévodkyně a kuchařka (1983)

### Současníci
Ota Pavel, Bohumil Hrabal

## Spalovač mrtvol

### Žánr
psychologický thriller, fikce

### Vyšla
1967

### Děj
Odehrává se v Praze v období krátce před a během protektorátu. Roman Kopfrkingl je manželem židovky Marie, otcem dvou dětí, zaměstnancem pražského krematoria. Pan Kopfrkingl se zpočátku jeví jako citlivý, starostlivý člověk, nekuřák, abstinent, buddhista. Během okupace ale pod vlivem svého přítele Waltra postupně mění své smýšlení, upravuje žebříček hodnot, až je nakonec schopen stát se členem SdP, NSDAP, udávat, vraždit a podílet se na výstavbě plynových komor.

### Postava #1
*Roman Kopfrkingl* je psychopat, uzavřený v kleci svých humanistických postojů, kterým natolik věří, že je nedokáže rozeznat od svého skutečně bezcitného nitra. Tento rozpor z něj proniká ve formě fascinace kultem smrti. Příchod protektorátu v něm probouzí bestii, která pod rouškou mesiášského komplexu páchá nejhorší zločiny.

### Postava #2
*Marie Kopfrkinglová*, rozená Šternova, tichá, nevýrazná až zakřiknutá manželka. Do vývoje děje se nesnaží zasahovat, tiše očekává svůj osud poloviční židovky provdané za němce. Přesto svého muže hluboce miluje, neopouští ho a věří mu.

### Jazyk díla
Dobová čeština, místy němčina, Er-forma, časté užití přechodníků, zvláštních pojmenování (Lakmé, čarokrásná, …), opakujících se motivů (růžolící dívka v černých šatech).

### Hlavní idea díla
Podobenství rozkladu morálních hodnot běžného člověka ideologiemi.

### Názor na dílo
Užívala jsem si narůstající skličující atmosféru i postupný rozklad morálních hodnot pana Kopfrkingla. Kniha je hluboce strukturovaná, postavy mistrovsky vylíčené. Misty mi vadily groteskní scény a postavy.

### Film
1968 režisér Juraj Herz


### PRIVATE
#### Děj detailně
Roman a Lakmé jsou manželi již 17 let. Mají dceru Zínu a mladšího syna Milivoje. Žijí v útulném pražském bytě. Pan Kopfrkingl se jeví jako velmi citlivý muž, starostlivý otec, milující manžel a příkladný zaměstnanec pražského krematoria, který svou práci vnímá spíše jako poslání - pomáhá duším vzlétnout do éteru, převtělit se. Ve volných chvílích se věnuje rodině, četbě knihy o buddhistickém Tibetu a poslechu vážné hudby. Že ale není s panem Kopfrkinglem něco v pořádku, tušíme z jeho morbidní zálibě ve funerální tématice, kterou omílá při každém rozhovoru. Podezření však dostává konkrétní podobu, jakmile Lakmé zmíní, že si její muž jména vymýšlí: ona se jmenuje Marie, on Roman a že si rád vymýšlí.

Pan Kopfrkingl pravidelně rozmlouvá se svým přítelem Walterem Reinke. Hned při první návštěvě zjišťujeme, že se blíží zábor Sudet a Willi je zanícený sympatizant Hitlera, který se v panu Kopfrkinglovi snaží probudit hlas kapky německé krve. Z každé jejich diskuse si pan Kopfrkingl odnáší něco z nacistických idejí o nadřazenosti germánské krve. Protižidovské a militantní postoje se mu pomalu daří skloubit s jeho buddhistickou koncepcí neubližování. Během pár let se tak pan Kopfrkingl stává členem SdP, NSDAP a nakonec člověkem natolik posedlým svou představou o osvícení a spasení, že se neštítí účasti na výstavbě plynových komor a pecí v koncentračních táborech, nebo dokonce vyvraždit svoji rodinu, protože mají židovské předky po matce a syn je navíc homosexuál.

Po kapitulaci Německa vyvrcholí mesiášský blud pana Kopfrkingla halucinací, v níž je navštíven tibetským mnichem a označen za další inkarnaci Buddhy. Ve skutečnosti je však odvezen sanitkou, nejspíše do blázince.

#### Interpretace
Často čtu, že novela je o postupném rozvratu osobnosti. Myslím ale, že ústředním a obecným motivem je spíše analýzla rozkladu morálních hodnot běžného člověka ideologiemi. Vzestup Třetí říše by nebyl možný bez spoluúčasti milionu obyčejných lidí, které nelze označit za psychopaty. Proto autor knihu uvádí citátem Giovanni Papiniho: „Největší lstí ďábla je, když sám o sobě prohlašuje, že není.“ Proto pan Kopfrkingl hned nepřijímá ideu árijské nadřazenosti, ale šetrně ji roubuje na mírumilovné buddhistické teze o neubližování a reinkarnaci. K udávání a vraždám se uchyluje teprve, až se zbaví kognitivních disonancí. On opravdu věří, že činí dobro.

#### O díle
Kniha vyšla roku 1967 v první tvůrčí etapě Ladislava Fukse. O rok později ji na filmové plátno kongeniálně převedl Juraj Herz. Dílo je psáno v Er-formě, jsou zde časté přechodníky opakování myšlenek, bizarní kombinace vět, zdlouhavé monology. Dějem provází vypravěč, který popisuje myšlenkové pochody pana Kofrkingla. V textu se vyskytují odkazy a paralely na nadcházející vraždy (Oběšenec na začátku v panoptiku - oběšení Lakmé. Roman oponuje Willimu, že jediný prostor, který člověk má, je prostor uvnitř rakve - umístění synova těla do rakve s tělem důstojníka), kterými autor ve čtenáři buduje soustavnou ostražitost a nedůvěru k ústřední postavě (K. se nechává testovat na pohlavní choroby - zjevně lže, že se s jinou ženou nestýká).

#### Výpis z kapitol
1. Pan Kopfrkingl bere rodinu do ZOO, kde se před 17 lety seznámil se svou ženou. Panu Štrausovi nabízí spolupráci, aby mu pomáhal roznášet reklamu na žeh.
2. Groteskní kapitola. K bere rodinu do panoptika madame Tussaud, kde shlédnou výjevy z období morových ran kolem roku 1680. To ho zavádí ho k přesvědčení, že dnešní doba taková utrpení téměř vymítila. Působí jako citlivý, snaživý, pečlivý člověk. S oblibou mluví o své práci v krematoriu a se zalíbením interpretuje život podle buddhismu, o kterém se dočetl v jeho nejoblíbenější knížce o Tibetu.
3. Zve na večeři známé, manželé Reinkeovi. Willi Reinke se po krátké době projevuje jako sympatizant Hitlera. Vychází najevo, že zrovna bylo obsazeno Rakousko. Vybízí pana K. ke vstup do SdP. Ačkoli je pan K. s spíše zdrženlivý, po odchodu Willyho přeci jen jeví znaky indoktrinace.
4. K. provází nováčka krematoriem, chrámem smrti. Pracuje zde 15 let. Jeho verze buddhismu dává jeho práci hlubší význam.
5. Je na návštěvě u doktora Bettelheima. Pravidelně se nechává testovat na pohlavní choroby. Prý ze strachu před nákazou z krematoria, i když nákaza není možná. Prý by se musel zastřelit, kdyby Lakmé ublížil. Doktor je Žid a humanista. Vláďa ČR vyhlašuje výjimečný stav v pohraničí. V Norimberku se koná sjezd, kde Hitler mluví útočně proti České Republice.
6. V chrámu smrti se dozvídá, že jedna z uklízeček odchází, protože ji práce v krematoriu vyděšená děsí. Kopferkingelovy monology se prodlužují, nabalují na sebe myšlenky náhodných, se kterými kdysi mluvil. Nechává si zarámovat zákon o kremaci, aby si jej vyvěsil doma. Byt zdobí předměty s funerální tématikou.
7. Zábor Sudet. K. se zajímá o výzdobu bytu. Zina zve svého chlapce na rodinnou oslavu svých narozenin. Představuje ho poprvé rodině. K. vede normální monolog, poté ale opět sklouzne řečí ke kremaci.
8. Willi zve K. a Miliho na boxerské utkání, aby viděli mužný sport, který je vůdci nejmilejší. Willy pokračuje v indoktrinaci, začíná mluvit potichu německy. Na konci kapitoly se zdá, že K s Willim nesouhlasí.
9. Vánoce. K. není sto zabít kapra. Před večeří přichází Willi, opět promlouvá ke K. Ten si osvojuje myšlenku, že je vyvolený. Rodině dává dárky, které v sobě opět nesou funerální symboliku.
10. K. už tolik nemyslí na převtělování a neomílá dokola vzpomínky na různé lidi. Dokonce přichází s myšlenkou, že by někomu ublížil. Willy ho pověřil úkolem - má se převléci za žebráka a jít vysledovat rozhovory Židů sázejících v židovské radnici na svátek Chevra Suda (narození a smrt Mojžíše, 6. března 1939) K. je členem SpD.
11. 15.březen. Okupace. Hitler přijel do Prahy. K je už i členem NSDAP. Kolem 20. dubna je K. pozván do kasina, kam se tolik toužil dostat. V kasínu, opojen vjemy a řečmi, udává kolegy Zajíce a Berana a především ředitele krematoria. Nedělá to ale s přímým úmyslem zlého. Jen přesně uvádí fakta. Na konci kapitoly je K. nabídnuta vysoká funkce, ale je mu vysvětleno, že překážkou je jeho žena Lakmé. Je Židovka, za svobodna Šternova. Karel nabízí rozvod.
12. Bere rodinu na petřínskou rozhlednu. Groteskní kapitola.
13. Milivoj a Zina odjíždí na návštěvu ke slatiňanské tetě. Karel doma oběsí Lakmé v koupelně se slovy: “Co abych tě, drahá, oběsil?” (strana 123). Přitom hraje Donizettiho Lucie z Lammermooru. Vraždu u policie vysvětluje jako sebevraždu, prý Lakmé nesnesla žít vedle Němce.
14. V kasínu Willi panu K. sděluje, že Miliho nevezmou do německé školy, protože je ein Vierteljude. Navíc zdá se i homosexuál. A tak bere syna na prohlídku do krematoria kde ho utluče železnou tyčí a jeho tělo zavře do rakve SS- důstojníkem. Smrt sbližuje.
15. Karel je poslán do Sicherheitsdienstu (bezpečnostní služba) před tajemníka říšského protektora Boehrmanna. Zde je mu sděleno, že bude jmenován odborným ředitelem tajného experimentu při výstavbě plynových komor a kremačních pecí. Doma, když je Zína v německé škole, má halucinace, že je navštíven buddhistickým mnichem a označen za inkarnaci Buddhy, za rimpočeho. Okamžitý odchod Lhasy ale odmíta, máš v plánu ještě v plánu zavraždit Zínu. Včas pro něj ale přijedou zřízenci blázince. Píše se rok 1945, je po válce, Židé se vracejí zbídačení z koncentráků, pan K. pozoruje jejich průvod a věří, že jim pomohl k očistě a spáse.

#### Zdroje
* Fuks, Ladislav. 2017. Spalovač mrtvol. Praha: Odeon.
* Wikipedie: Otevřená encyklopedie: Spalovač mrtvol (novela) [online]. c2018 [citováno 24. 02. 2019]. Dostupný z WWW: <https://cs.wikipedia.org/w/index.php?title=Spalova%C4%8D_mrtvol_(novela)&oldid=16468059>
Spalovač mrtvol. 1968 [film]. Režie Juraj Herz. Československo
