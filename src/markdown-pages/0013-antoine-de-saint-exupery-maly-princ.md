---
title: "Malý princ"
writer: "Antoine de Saint-Exupéry"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Antoine de Saint-Exupéry, Malý princ"
description: "Čtenářský deník k povinné maturitní četbě Antoine de Saint-Exupéry - Malý princ"
slug: "/literatura/0013-antoine-de-saint-exupery-maly-princ/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Antoine de Saint-Exupéry

### Celé jméno
oficiálně Antoine Marie Jean-Baptiste Roger, hrabě de Saint Exupéry

### Národnost
francouzská

### Profese
spisovatel

### Život
Narodil se 1900 v Lyonu. Studoval v Paříži a ve Švýcarsku. Během vojenské služby po 1. světové válce získal pilotní licenci a letectví se pak věnoval celý život, v Africe byl ředitelem pouštního letiště. Po vypuknutí 2. světové války získal povolení létat průzkumným letadlem. Po nehodě odjel do Alžíru, kde chtěl dokončit svoji další knihu. Dostal povolení ještě létat, letěl devětkrát, 31. července 1944 vzlétl z Korsiky k letu nad Francií, ze kterého se již nevrátil. O svých leteckých zážitcích začal psát knihy.

### Další díla
Kurýr na jih (1929), Noční let (1931), Země lidí (1939)

### Současníci
Romain Rolland, Henri Barbusse

## Malý princ

### Žánr
pohádkový příběh (epický, fiktivní)

### Vyšla
1943

### Děj
Vypravěč začíná diskusí o povaze dospělých a jejich neschopnosti vnímat zvláště důležité věci. Jako test k určení, zda je dospělý osvícený a jako dítě, ukazuje jim obrázek, který nakreslil ve věku 6 let a zobrazoval hada, který snědl slona. Dospělí vždy odpovídají, že na obrázku je klobouk, a proto ví, že o nich mluví „o rozumných“ věcech, než o fantazii.

Vypravěč se stal pilotem a jednoho dne jeho letadlo havaruje v Sahaře , daleko od civilizace, kde usnul v písku a ze sna jej probudí malý princ a chce nakreslit beránka. malý princ začal vyprávět o svých cestách po planetách , sám pochází z malé domovské planety, ve skutečnosti je to asteroid velikosti domu známý jako “B 612” na Zemi. Princ popisuje jak trávil své dřívější dny, jak pečuje o svou květinu, která se nebála tygrů, ale průvanu. Jak také na malé planetě vytrhával baobaby, aby neroztrhaly svými kořeny celou planetu, ale také dával pozor, aby mu je jeho beránci neokusovali. Další dny malý princ vyprávěl o svých cestách po šesti dalších  planetách, z nichž každá byla osídlena jediným dospělým, z nichž každá měla kritizovat prvek společnosti. Na první byl pyšný král, který vydává pouze rozkazy, jako je třeba přikázání západu slunce zapadnout. Na druhé zas byl domýšlivec (narcistický muž), který chce pouze chválu, která přichází z obdivu, ale nemá ho kdo obdivovat. Třetím samotářem byl pijan, který pije, aby zapomněl na hanbu pití. Na čtvrté je bohatý podnikatel, který je slepý vůči kráse a místo toho je nekonečně počítá a zaznamenává, aby je všechny “vlastnil”. Na páté planetě je lampář, který musí stále pracovat, protože celý den mu trvá jen minutu, plýtvá životem podle pokynů, aby každých 30 sekund zhasl a rozbil lampu. Šestá planeta byla desetkrát větší a byl na ní zeměpisec, který nikde nikdy nebyl, nebo neviděl žádnou z věcí, kterou zaznamenal do veliké knihy vzpomínky ostatních badatelů, kteří ho navštívili a přinášeli mu důkazy, že sami mluví pravdu. A konečně sedmá planeta  byla Země, na které začíná návštěva hluboce pesimistickým hodnocením lidstva, a na kterou princ spadl a potkal žlutého hada, který rozumí všem hádankám, také potkal pyšnou růži a vystoupal na nejvyšší horu, jakou kdy viděl, a doufal, že uvidí celou Zemi, a tak najde lidi. Lehl si na trávu a plakal, dokud nepřišla liška. Liška chtěla být zkrocena. Vysvětlovala princi cenu a smysl přátelství i to, že kdyby si ji ochočil, byl by nesl za ni zodpovědnost. Od lišky se dozví, že jeho růže byla jedinečná a zvláštní, protože byla předmětem princovy lásky a času. Princ konečně potkal dva lidi ze Země, výhybkáře, obchodníka.

A teď je tu ta chvíle - ten osmý den, kdy vyprávěl pilotovi. Realita s nedostatkem pitné vody a opravováním letadla je zapomenuta - opravuje se tu pilotova porouchaná touha žít. A tak jdou spolu hledat studnu. Našli ji a napojili se vodou. Poté princ pošle pilota zpět k letadlu, které se mu podařilo opravit, a následující den odcestuje. Teď může malý princ odejít, protože už napořád zůstane v srdci svého velkého člověka. Po dobrovolném uštknutí hadem padá bezhlučně do trávy. Princ umírá v poušti dospělých. Hvězdy už budou vždycky znít “jako pět set milionů rolniček”.


### Postava #1
Mladý chlapec přezdíván jako *“malý princ”* - opakuje neustále otázky, dokud mu nejsou zodpovězeny.

### Postava #2
*Autor* - osamělý vypravěč, dospělý muž, pilot s “porouchaným motorem”, který uvízl v srdci saharské poušti a je pro něj “otázkou života a smrti” jestli dokáže své letadlo sám opravit. Dospělý muž je okouzlen dítětem v sobě, jeho svobodou, fantazií a nesmírnou vážností, kterou vyžaduje pozornost.

### Jazyk díla
Překlad od Zdeňky Stavinohové, spisovný jazyk, velice jednoduchý a nenáročný, velké množství metafor a přirovnání, alegorie (skrytý význam), občas také personifikace, výrazná symbolika. Ich-forma.

### Stylistická charakteristika textu
součástí knihy jsou i autorovy vlastní ilustrativní obrázky

### Hlavní idea díla
Je sice pohádkovou knihou určenou primárně dětem, ale využívá postupů blízkých bajkám, a tak  k pochopení využívá i zkušenost, která přichází s přibývajícím věkem. Svět dětí je i střetem se světem dospělých s fantazií, ale očima dospělého a rozumem jsou vnímány pouze realisticky. Kniha je věnována autorovu příteli Léonu Werthovi, když byl ještě malým chlapcem.

### Názor na dílo
Celý příběh je vlastně jednou velkou otázkou, tou největší ze všech: co je to lidský život a jak s ním nejlépe naložit.

### Film
dílo bylo několikrát zfilmováno včetně filmového muzikálu, dvou oper a animovaného seriálu
