---
title: "Kytice"
writer: "Karel Jaromír Erben"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Karel Jaromír Erben, Kytice"
description: "Čtenářský deník k povinné maturitní četbě Karel Jaromír Erben - Kytice"
slug: "/literatura/0020-karel-jaromir-erben-kytice/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Karel Jaromír Erben

### Národnost
česká

### Profese
spisovatel, básník, prozaik, historik, překladatel, sběratel lidových písní, básní
a pohádek

### Život
Narodil se 1811 v Miletíně v řemeslnické rodině. Studoval gymnázium v Hradci Králové. Poté filozofii a práva v Praze. V mládí se seznámil s F. Palackým (odtud zájem o historii a národopis). Byl ustanoven sekretářem. V roce 1851 zvolen archivářem města Prahy. Roku 1853 poprvé vydává svoji autorskou sbírku Kytice z pověsti národních, na které pracoval 20 let. Podílel se na vydávání časopisů Obzor a Právník. Zemřel 1870 v Praze.

### Další díla
Prostonárodní české písně a říkadla, Sto prostonárodních pohádek a pověstí slovanských v nářečích původních(1805)

### Pohádky
Tři zlaté vlasy děda Vševěda, Pták ohnivák, Zlatovláska

### Současníci
Karel Hynek Mácha, Josef Kajetán Tyl

## Kytice

### Žánr
staré lidové báje, české i německé

### Vyšla
1853, rozšířené vydání 1861 - Jediná sbírka básní, kterou K. J. Erben vydal. Sbírka obsahuje 13 básní a pouze první a poslední nejsou balady.

### Děj
Úvodní báseň *Kytice* vypráví pověst o duši mrtvé matky, která se vtělila v kvítek, aby potěšila opuštěné děti. Druhá část vysvětluje poslání knihy. Na hrobě matky - vlasti - nachází básník její odkaz pro děti - národ, lidovou slovesnost.

Závěrečná *Věštkyně* se skládá z úryvků šesti lidových pověstí o české zemi. V baladách pak řeší autor problém viny a trestu nebo snahy vzepřít se osudu.

V *Pokladu* matka na Velký pátek najde otevřenou skálu s pokladem, když vynáší zlato a stříbro, skála se zavře i s dítětem, svou vinu se snaží vykoupit lítostí a pokáním, skála jí ho za rok vydá.

Ve *Svatební košili* bojuje dívka svojí vírou se svým milým, který za ní přišel jako umrlec.

V *Polednici* matka vyhrožuje a volá ve zlosti polednici, sama podlehne strachu a dítě nechtěně udusí.

*Zlatý kolovrat* vypráví o Doře, do které se zamiluje král ze zámku, nevlastní matka však chce provdat druhou dceru, proto společně cestou v lese Doru zabijí, odeberou jí nohy, ruce a oči. Král nepozná rozdíl, vezme si druhou dceru a odjíždí do boje. Stařeček v lese nachází tělo a posílá páže do zámku prodávat zlatý kolovrat, přeslici a kužel za nohy, ruce a oči. Ty spojí s tělem a Dora ožije Když chce sestra pánovi něco upříst, kolovrátek mu prozradí celý příběh. Pán Doru najde a vezme si ji. Matce a sestře udělá to, co ony Doře.

V básni *Štědrý den* se Marie a Hana jdou podívat v době adventu na jezero, vysekají díru do ledu, aby se jim zjevil ženich. Haně se zjeví myslivec Václav - vdá se, Marii však jen kostel, svíce, rakev a do roka zemře.

*Holoubek* zpívá ve stejnojmenné básni ženě pravdu, že otrávila muže, ta vinu neunese a utopí se v řece, Je pochována u břehu bez pořádného hrobu. V Záhořově loži řeší autor snahu vytrhnout se z  moci pekla, což se daří jedině vírou a pokáním.

*Vodník* řeší rozpor mezi láskou k matce a  k vlastnímu dítěti. Když se dívka i přes matčino varování propadne do Vodníkova světa, stává se jeho ženou a má s ním i syna, její jediné potěšení. Vodníka přemluví, aby ji pustil rozloučit se s matkou, ale matka nechce dceru pustit. Vodník si pro ženu přijde, ale matka ji ani teď nepustí a Vodník se pomstí vraždou dítěte.

V básni *Vrba* se žena v noci vtěluje do vrby, manžel ji porazí, ale tím ženu zabije. Nakonec z vrby udělá kolébku, z jejíchž větví si chlapec bude dělat píšťalky a tak rozprávět s maminkou.

V *Lilii* nechtěla být dívka pohřbena na hřbitově, ale v lese, kde na jejím hrobě vykvetla lilie. Místní pán ji přenesl do své zahrady a zjistí, že lilie v noci ožívá. Chce ji chránit před světlem, ožení se s ní a mají syna. Když musí do boje, prosí matku, aby ji ochraňovala. Ta však nechá světlo, aby matku i dítě zabilo.

V *Dceřině kletbě* rozmlouvá s matkou dcera, která zabila své dítě, proklíná svého milého a matku, že za její neštěstí mohou oni a od žalu jí může pomoci jen oprátka.

### Postavy
Většinou převažují ženy (např. matka, dcera nebo syn, apod.) Dále některé pohádkové a nadpřirozené postavy (polednice, vodník, aj.)

### Jazyk díla
spisovný (zastaralý), baladická zkratka = zhuštěný a úsporný jazyk, často neslovesné věty bez spojek, citoslovce, dějový spád a dramatičnost, zvukomalba, naléhavost a mystičnost (pohádkové a kouzelné prvky), časté personifikace nebo metafory.

Autor - vnější nezávislý pozorovatel děje (er-forma), časté střídání s přímou řečí (monologem) jednotlivých postav.

### Hlavní idea díla
Připomenutí pradávných morálních zásad, zodpovědnost za vlastní chyby. Za chyby vždy přichází trest. připomínka lidové slovesnosti (starých lidových pověr a zvyků). Morální zákony lze překonat pouze pokáním, odpuštěním nebo láskou.

### Názor na dílo
“Za chybné chování vždy přichází trest”

### Film
* Kytice - Česká republika, 2000 - režie: F. A. Brabec
* Divadlo Semafor: Kytice - 2005 - Představení z r. 1973 na motivy Erbenovy Kytice

