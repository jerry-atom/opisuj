---
title: "Oliver Twist"
writer: "Charles Dickens"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Oliver Twist, Charles Dickens"
description: "Čtenářský deník k povinné maturitní četbě Charles Dickens - Oliver Twist"
slug: "/literatura/0007-charles-dickens-oliver-twist/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-20"
dateModified: "2021-02-20"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Charles Dickens

### Národnost
britská

### Profese
spisovatel

### Život
Narozen 1812 v Porhmuthu do chudé rodiny. Talentované dítě. Otec uvězněn za dluhy a tak Charles musel od 10 let pracovat. To hluboce ovlivnilo jeho vnímání světa a tvorbu. Vypracoval se z továrny na leštidla na boty, přes pomocníka v právnické kanceláři až na redaktora v novinách. Stal se úspěšným spisovatelem. Měl 10 dětí. Zemřel na srdeční příhodu v r. 1870 v Gadshill.

### Další díla
Kronika Pickwickova klubu (1837), Vánoční koleda (1843), David Copperfield (1849), Malá Dorritka (1857).

### Současníci
Gustav Flaubert, Honoré de Balzac, Fjodor Michajlovič Dostojevskij.

## Oliver Twist

### Žánr
román

### Vyšla
1838, česky 1844

### Děj
Oliver Twist ihned po porodu osiří a vyrůstá v obecním sirotčinci, kde jsou děti zanedbávány na výživě, hygieně a vzdělání. Mnoho z nich umírá. O. je dětmi vylosován, aby požádal o vyšší příděl jídla. Za to jej pan Brownlow exemplárně potrestá a umístí do učení v pohřebnictví pana Sowerberryho. Zde je šikanován jeho ženou a dalším učněm a tak uteče. Na pokraji sil se dostává do Londýna, kde se ho ujme Žid Fagin, vůdce dětských kapsářů, jako dalšího kadeta. O. si neuvědomí, že je mezi zloději a při první výpravě je proto omylem dopaden místo pravých lupičů. Je zbit, uvězněn a téměř odsouzen ke 3 měsícům tvrdé práce. Těžce nemocného O. se ujme bohatý pan Brownlow, vyléčí ho a chce se ho pečovat jako vlastního syna. O. by rád, ale je zajat partou Fagina. Při další výpravě je O. postřelen a ošetřen paní Rózou Maylieovou. Ve Faginově partě je Edward Leeford, který si uvědomí, že je O. nevlastním bratrem a že se musí nic netušícího O. zbavit, aby získal jeho dědictví. Zničí přívěsek, který odkazuje na O. původ, ale svěří se s tím Faginovi. To vyslechne zlodějka Nancy, která to prozradí Róze, aby O. pomohla. Když se to v partě dozví, zabijí ji. Róza tedy vše vyloží O. a ten jde za panem Brownlowem, který mu shodou náhod odhalí, že mu a Edwardovi otec odkázal velké jmění. Edward dědictví rozhýří v USA a zemře ve věznici. Fagin je chycen a oběšen, pan Bumbl zbaven funkce a internován ve špitále. Oliver je adoptován panem Brownlowem a konečně nachází pokojný život.

### Postava #1
Oliver Twist - navzdory dětství v bídě a bez lásky má dobrotivý charakter, je inteligentní, překvapivě naivní a hlavně má nesmírné štěstí

### Postava #2
Žid Fagin - jako vůdce gangu dětských zlodějíčků nemůže být než zákeřný, lstivý a vychytralý

### Jazyk díla
Kniha členěna na 53 krátkých ostře pointovaných kapitol. Autorský vypravěč provází příběhem a vysvětluje detaily příběhu a pohnutky postav. Spisovný a vytříbený slovník, v nepřímé řeči se jazyk liší podle společenského umístění mluvčího.

### Hlavní idea díla
Kitika společenských poměrů, zejména postoje společnosti k sirotkům.

### Názor na dílo
Podmanivé a pestré vyprávění. Šokující sonda do viktoriánské Anglie. Oliverovo štěstí nepůsobí uvěřitelně. Autor vidí lidi buď dobré, nebo zlé, nic mezi tím.
