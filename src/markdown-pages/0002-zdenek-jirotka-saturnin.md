---
title: "Saturnin"
writer: "Zdeněk Jirotka"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Zdeněk Jirotka, Saturnin"
description: "Zápis ze čtenářského deníku k povinné maturitní četbě knihy Saturnin od Zdeňka Jirotky"
slug: "/literatura/0002-zdenek-jirotka-saturnin/"
authorName: "@sari"
dateCreated: "2020-06-14"
dateModified: "2020-06-14"
dateMublished: "2020-06-14"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Zdeněk Jirotka

### Národnost
česká

### Profese
spisovatel

### Život
Narozen 1911 v Ostravě, vyloučen z gymnázia, dostudoval stavitelství na SŠ. Do okupace armádní důstojník. Od 1942 spisovatel, později redaktorem časopisu Dikobraz. Umírá 2003 v Praze.

### Další díla
Muž se psem (1944), Hvězdy nad starým Vavrouchem (1946), Pravidla se změnila (2000)

### Současníci
Franz Kafka, Karel Čapek, Karel Poláček

## Saturnin

### Žánr
próza

### Vyšla
1942

### Děj
Čas děje není uveden a není podstatný, zřejmě období První republiky (1918-1938). Vypravěč vzpomíná, jak jeho pohodlný život změnilo výstřední rozhodnutí najmout si sluhu. Sluha Saturnin má v oblibě vystavovat lidi absurdním situacím. Svého pána bez jeho vědomí přestěhuje na hausbót na Vltavě, ten to akceptuje bez protestů. Jejich idilu naruší teta Kateřina, která se se synem Miloušem vetře na stísněnou palubu. Saturnin demonstruje své schopnosti a tetu galantně donutí odejít. Později oba odjíždí strávit dovolenou k dědečkovi do blíže neurčeného podhůří. Dědeček je velmi bohatý, bývalý stavitel elektráren. Náhodou je pozvána i slečna Barbora, se kterou se vypravěč dříve seznámil a tajně po ní touží. Přichází i rodinný přítel doktor Vlach a opět se vetře bez pozvání teta Kateřina s Miloušem. Zápletku otevírá bouře, která rozvodní říčku, ta strhne most a všech 7 lidí odřízne od nedalekého městečka. Saturnin v bouřce potají vypíná hlavní jistič. V domě, kde je všechno na elektriku, takže nelze přivolat pomoc telefonem ani uvařit. Vypnutý jistič uniká pozornosti po 2 dny díky víře, že elektřinu přerušila bouře. Trosečníci musí řešit nevšední situace, během nichž se prohlubuje vztah mezi Barborou a vypravěčem, nebo se ukazují pravé úmysly tety Kateřiny (chce dědictví od dědečka). Úžící se zásoby jídla přinutí skupinu uspořádat pochod na nedaleký srub doktora Vlacha a odtud obejít řeku a ubytovat se ve městečku. Pochod se ale protáhne na dva dny, neboť se dědeček po cestě zraní. Po návratu z městečka zjišťují, že most byl mezitím opraven. Všichni se vrací zpátky domů. Dědeček sepíše poslední vůli, kde vše odkáže na dobročinné účely, což rozhněvá tetu Kateřinu. Vypravěč v Praze pokračuje se slečnou Barborou. A dědeček si Saturnina tak oblíbil, že požádá, aby mu jej vypravěč přenechal.

### Postava #1
*vypravěč (bez jména)* - 30 let, dobře situovaný nezadaný úředník dbalý etikety, konzervativní a neprůbojný

### Postava #2
*Kateřina* - stárnoucí vypravěčova teta, vdova s dospívajícím synem, sobecká, manipulativní, nezodpovědná

### Jazyk díla
autorské chronologické vypravování, ich-forma, převážně spisovná čeština, více nepřímé a polopřímé řeči než přímé, několik vložených dopisů a vypravování postav

### Hlavní idea díla
Humorné vyprávění stavící na rozdílnosti charakterů vystavených tísnivé situaci.

### Názor na dílo
TV seriál Saturnin (1994, režie Josef Věrčák) a zkrácená verze jako samostatný film. Kniha byla příjemnou zábavou. Pasáže, které až zbytečně prodlužovaly děj.

### Podrobnější děj
Vypravěč si najme sluhu, Saturnina. Od první chvíle se vypravči Saturnin zdá velmi zvláštní, což se za nedlouho potvrdí, protože mu volá do kavárny, že jeho byt byl přestěhován na hausbót. Na kurtech potkává slečnu Barboru, která na rozdíl od něho umí skvěle hrát tenis, takže mu Saturnin na lodi postaví cvičnou tenisovou zeď, aby se mohl zlepšit a být rovnoceným soupeřem. Je až záhadou jak přesné informace se Saturninovi dostávají. Klidný život je ovšem vyrušen tetou Kateřinou, která přijela se svým synáčkem Miloušem za nimi na návštěvu, to se ovšem vypravěči nelíbí, protože teta se bez dovolení nastěhovala do jeho kajuty, takže nemá kde spát. Saturnin to pozná, takže je začne svým podivným způsobem vyhánět z lodi pomocí škrabošky čerta a Mikuláše. Za nedlouho přicházejí prázdniny, které se vypravěč rozhodl strávit se Saturninem na venkově ve vile dědečka. Tam se setkává s doktorem Vlachem, slečnou Barborou, ale také s tetou Kateřinou a jejím povedeným synáčkem Miloušem. Tam se odehraje několik příhod, které jsou většinou způsobeny Saturninem, ale to se dovídáme zajímavým způsobem většinou na konci příběhu. Příjemná dovolená končí velkými deštmi, kdy dojde k výpadku elektrického proudu a k vypnutí jističe, na čež je tato skutečnost zapomenuta. Protože dědeček je velmi zamilován do elektáren a do všeho elektrického, má ve vile vše ektrifikováno, takže v podstatě nemůžou  v domě téměř nic využít – i vařit musí slečna Barbora na otevřeném ohništi za vilou. Záplavami je stržen most vedoucí jako jediná přístupová cesta k městu, proto se ocitají téměř odříznuti od světa. Hospodyně s kuchařkou jely těsně před spadnutím mostu na nákup, zásoby jídla se ztenčují, takže po několika dnech se rozhodli vydat na chatu doktora Vlacha, která leží v kopcích půl dne cesty od  vilky a potom dále do města. Cesta však neplánovaně trvala tři dny. První den byl pořád do kopce na chatu doktora Vlacha, tam přespali a druhý den se vydali dál, dědeček si však nešťastnou náhodou zvrtne kotník a musí jít velmi pomalu.  Neplánovaně přespávají na louce. Zcela nepochopitelně je Saturnin připraven i na tuto skutečnost a  vytahuje ze svého batohu pro všechny zúčastněné deky. Třetí den docházejí opět k blízkosti vilky, protože museli s ohledem na dědečkovo zranění změnit trasu, a vidí na místo, kde za normálních podmínek stojí most a co tam nestojí? Ano byl tam most! Všichni byli unavení, takže jenom celí neštastní, protože cesta byla zbytečná, došli k vile a lehli si do svých postelí. Na konci prázdnin všichni odjíždí do svých domovů, až na Saturnina, který se spřátelil s dědečkema a zůstává mu dělat společníka. Ke konci knihy si pak přečteme dva dopisy, které jsou psány dědečkem a Saturninem. Vypravěč si také domlouvá schůzku se slečnou Barborou. Na úplném konci příběhu se odehrává svadba, kdy očekáváme, že se provdá slečna Barbora s vypravěčem, ale překvapení na nás čeká i nyní – bohatě se provdává teta Kateřina.
