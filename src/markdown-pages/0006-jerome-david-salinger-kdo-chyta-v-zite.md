---
title: "Kdo chytá v žitě"
writer: "Jerome David Salinger"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Kdo chytá v žitě, Jerome David Salinger"
description: "Čtenářský deník k povinné maturitní četbě Jerome David Salinger - Kdo chytá v žitě"
slug: "/literatura/0006-jerome-david-salinger-kdo-chyta-v-zite/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-02-20"
dateModified: "2021-02-20"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Jerome David Salinger

### Národnost
američan

### Profese
spisovatel

### Život
Narozen 1919 v New Yorku do rodiny židovského obchodníka polského původu. Měl sestru Doris. Pokoušel se stát hercem a obchodníkem. Bojoval v 2.S.V., kde se spřátelil s E. Hemingwayem. Později se věnoval už jen spisovatelské činnosti a stranil se společnosti. Zemřel v r. 2010 v USA.

### Další díla
Devět povídek (jedna z nich je Den jako stvořený pro banánové rybičky, jejíž název se stal předlohou pro TV pořad Haliny Pawlowské), Franny a Zooey.

### Současníci
E. Hemingway, William Styron, Joseph Heller.

## Kdo chytá v žitě

### Žánr
román

### Vyšla
1951

### Děj
17letý Holden Caulfield v léčebně vypráví svůj příběh. Zrovna před vánoci 1949 ho vyloučili ze střední školy v Pencey za špatné výsledky. Má v plánu přijet domů za rodiči a pak zmizet ještě dřív než jim dojde oznámení o vyloučení. Mezitím se ale nepohodne se spolužákem Stradlaterem, s nímž navíc prohraje pěstní souboj kvůli dívce. To přiměje H. odjet do New Yorku za peníze z prodeje svého psacího stroje. Cestou si vylévá svou zlost lhaním. V metropoli se pokouší mezi náhodnými lidmi najít spřízněnou duši výzvami k intelektuálním rozhovorům. Všichni jej ale ignorují: taxikář, tanečnice a nakonec i prostituka, která se domáhá odměny za svůj čas zmařený H-ovým tlacháním. H je zbit jejím pasákem. Druhý den si domluví schůzku se Sally Hayesovou. Vše se vyvíjí slibně, dokud Sally nepředstaví svého přítele George. To jej raní, protože ji chtěl pro sebe, a přehnaně ji vyzývá, aby s ním utekla do divočiny v New England. Když Sally samozřejmě odmítne, rozejdou se a H. se opije. Další noc se vkrade do bytu rodičů, aby se potkal se milovanou sestrou Phoebe. Ta mu vyčítá jeho lehkomyslnost a H. pokračuje ve fantazírování o hledání smyslu života. Před příchodem rodičů se z bytu vyplíží a najde přístřešek u svého bývalého učitele Antolliniho. Ten se ho ale pokusí svést a tak H uteče a noc stráví na nádraží, kde podléhá úzkostem a rozhodne se utéct před všemi a žít poustevnickým životem. Před tím se ale chce rozloučit se sestřičkou, která ho ale svým dětským způsobem přiměje zůstat. Nakonec H. vše přiznává rodičům, souhlasí s léčbou a nastoupí na další střední školu.

### Postava #1
Holden Caulfield - 17 letý student, velmi citlivý, bystrý, výbušný, miluje svou sestru a svou lásku k ní projektuje do svého vnímání světa.

### Postava #2
sestra Phoebe - 10 letá dívka, velmi inteligentní, paličatá, miluje svého bratra.

### Jazyk díla
Vyprávění ich-formou. Vulgarismy, slangové výrazy a hovorový jazyk. Silně subjektivní pohled vypravěče.

### Hlavní idea díla
Konfrontace ideálů dospívajícího mládence s bezohlednou a břitkou realitou světa.

### Názor na dílo
Hovorový a vulgární jazyk, kvůli kterému byla kniha často cenzurována. Otravné patálie sexuálně frustrovaného, narcistního a rozmazleného adolescenta. Název “Kdo chytá v žitě” pochází z básně Roberta Burnse, kterou Holden špatně pochopí. Vnímá sebe jako někoho, kdo chce chytat v žitě děti před tím, aby spadly ze srázu.
