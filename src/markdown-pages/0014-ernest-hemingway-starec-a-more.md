---
title: "Stařec a moře"
writer: "Ernest Hemingway"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, Ernest Hemingway, Stařec a moře"
description: "Čtenářský deník k povinné maturitní četbě Ernest Hemingway - Stařec a moře"
slug: "/literatura/0014-ernest-hemingway-starec-a-more/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## Ernest Hemingway

### Národnost
americká

### Profese
spisovatel, novinář

### Život
Narodil se 1899 v Illinois v USA do rodiny lékaře. Za první světové války bojoval jako dobrovolník na italské frontě. Válečné prožitky i záliba  v nebezpečí či extrémních sportech se promítla do jeho děl. Nositel Pulitzerovy ceny (1953) a Nobelovy ceny za literaturu (1954). Miloval ruské spisovatele. Poslední léta svého života strávil na Kubě. Zemřel v roce 1961 - pravděpodobně spáchal sebevraždu.

### Další díla
Fiesta (1926), Sbohem armádo (1929), Komu zvoní hrana (1940), Pátá kolona (jediné drama)

### Současníci
William Faulkner, John Steinbeck

## Stařec a moře

### Žánr
dobrodružná novela (próza, epika)

### Vyšla
1952

### Děj
Starý kubánský rybář Santiago už velmi dlouho neulovil rybu a živoří. Jeho mladý učeň a přítel, chlapec Manolin, proto musí jít lovit s úspěšnějšími rybáři. Santiago tedy riskuje a vesluje dál od pevniny do hlubinách Golfského proudu, kde jiní rybáři neloví. Tam se mu podaří zaseknout obrovského marlina. Po dvou dnech a nocích zápasu, do kterého Santiago vloží veškeré své zkušenosti a svou sílu, rybu konečně zdolá, zabije harpunou a se zkrvavenými dlaněmi přitáhne k lodi. Marlin je ale tak velký, že ho nemůže vytáhnout na loďku, musí ho vláčet na přídi. Po pár hodinách se ryba stává terčem žraloků, které se starci podaří zabít, avšak ztrácí harpunu a zbývá mu jako ochrana pouze nůž, který přiváže k pádlu, kdyby došlo k dalšímu napadení žraloky. Brání zbytky ulovené ryby. Po pár hodinách  dorazí do noční Havany, ale už jen s ohlodanou rybí kostrou. V závratích se dobelhá do své chatrče, kde hluboce usne. Probudí ho Manolin, kterému pak Santiago vypráví a popisuje co se, během tří dnů, na moři odehrávalo. Mezitím kolemjdoucí dav lidí obdivuje  kostru obřího marlína modrého.

### Postava #1
*Santiago* - starý, zkušený, nezlomný, kubánský rybář.

### Postava #2
*Manolin* - mladý chlapec, obdivuje Santiaga.

### Jazyk díla
Překlad od Zdeňka Vrby. Typicky úsporný jazyk, jednoduchý styl, který provází občas delší monology. Chronologické vypravování v ER-formě. Přítomny personifikace a slangové výrazy z rybářského slovníku.

### Hlavní idea díla
Minimalistická alegorie boje nezlomného člověka s přírodou.

### Názor na dílo
Užívala jsem si sugestivní popisy moře a líčení Santiagova vztahu k němu. Např. kontrast klidné hladiny moře s odlesky slunce, odvěké prázdnotě a mírnému houpání mocných vln. Hemingwaye fascinovali muži, kteří uměli dokázat svou cenu tím, že přijímali výzvy přírody.
