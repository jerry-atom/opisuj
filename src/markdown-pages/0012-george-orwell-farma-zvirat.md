---
title: "Farma zvířat"
writer: "George Orwell"
keywords: "čtenářský deník, maturita, literatura, rozbor díla, povinná četba, George Orwell, Farma zvířat"
description: "Čtenářský deník k povinné maturitní četbě George Orwell - Farma zvířat"
slug: "/literatura/0012-george-orwell-farma-zvirat/"
authorName: "@sari"
dateCreated: "2020-06-14"
datePublished: "2021-03-08"
dateModified: "2021-03-08"
organisation: "SOŠ Morava, Brno"
category: "maturitní četba"
---

## George Orwell

### Národnost
Britská

### Profese
novinář, spisovatel, prozaik, esejista

### Život
Vlastním jménem Eric Arthur Blair. Narozen 1903 (Motihari, Indie). Zemřel 1950 (Londýn) na tuberkulózu. Vystudoval prestižní Eton College. Po roce 1936 dobrovolník zapojil v španělské občanské válce, kde byl zraněn. Od roku 1940 pracoval pro BBC.

Orwellovy knihy a eseje se netýkají jen politiky, ale komentují život své doby a zabývají se sociálními tématy. Jeho tvorba je kritikou totalitních systémů (komunismus, fašismus).

### Další díla
1984, Na dně v Paříži a v Londýně, Nadechnout se

### Současníci
Světoví autoři: Isaac Asimov, Ray Bradbury. Čeští autoři: Ludvík Souček, Karel Čapek

## Farma zvířat

### Žánr
alegorický satirický román ve formě bajky (próza, epika)

### Vyšla
1945 (během 2. světové války)

### Děj
Vypráví o zvířatech, která obsadí Panskou farmu, kterou vlastní pan Jones. Svrhnou tyranii lidí, následně jsou však zrazena svými vůdci, jimž zachutná moc a nastolí novou, ještě přísnější diktaturu. Prase Major, dožívající kanec, si svolá zvířata z farmy a projevem o svobodě vyvolá revoluční atmosféru. Po jeho smrti se skupina jiných prasat ujme převratu. Zprvu hlásají, že všechna zvířata jsou si rovna. Pak ale prase Napoleon převezme s vycvičenými psy moc, vyžene do exilu prase Kuliše a k smrti uštve koně. O propagandu vlády zvířat se v novou tyranii stará prase Pištík, využívající dav tupých ovcí k šíření hesel. Postupně nahrazuje stará hesla novými (zvířatům tvrdí, že mají špatnou paměť), až to poslední nakonec zní: “Všechna zvířata jsou si rovna, ale některá jsou si rovnější.” V závěru se na společné hostině sejdou prasata s lidmi, přičemž ostatní zvířata to smutně pozorují s vědomím, že prasata jsou již stejná jako kdysi lidé.

### Postava #1
*Pan Jones* - původní majitel farmy, velký alkoholik, reprezentuje starý politický systém

### Postava #2
*Major* - starý inteligentní kanec, reprezentuje ideového zakladatele hnutí (např. Vladimir Iljič Lenin)

### Postava #3
*Napoleon* - nejchytřejší prase, podvodník a pokrytec, reprezentuje diktátora, který zneužije revoluce ve svůj prospěch (např. Josif Vissarionovich Stalin, Mao Ce-tung, aj.)

### Další postavy
Zpravidla reprezentují funkce v totalitním zřízení. Např. Kuliš, chytré prase, konkurence Napoleona, soustředí se na blahobyt lidí, nakonec musí být odstraněn (Trockij). Prase Pištík šíří propagandu. Kůň Boxer představuje lid, je pracovitý, čestný, nakonec je zlomen a umlčen.

### Jazyk díla
Spisovný. Mnoho slov se socialistickým zabarvením (Liga mládeže, Sportovní výbor). Vypravěčem je autor jako vnější nezávislý pozorovatel děje (er-forma). Výskyt metafor (psi jako tajná policie, kůň jako lid, prasata jako vládnoucí třída). Přímá a nepřímá řeč.
Hlavní idea díla: V díle jde o to, že i když je na začátku dobrá myšlenka, je zničena, pokud se někteří dostanou k moci. Dělají totiž vše jen ve svůj prospěch. Svého štěstí a majetku se snaží nabýt i “přes mrtvoly”. Tím pádem si mění pravidla podle svého.

Obecné vyjádření logického dějinného cyklu:
DIKTATURA → REVOLUCE → POSTUPNÝ NÁVRAT K DIKTATUŘE (pouze s jiným vedením)

### Názor na dílo
Fascinovalo mě, nakolik se Orwell trefil do vývoje totalitního režimu v Sovětském svazu a u nás. V roce 1945 knihu psal jako reakci na vývoj v Německu a teprve začínající vývoj v Sovětském svazu.

### Film
Farma zvířat (1954, animovaný, režie Joy Batchelor a John Halas), Farma zvířat (1999, televizní hraný film, režie John Stephenson).
