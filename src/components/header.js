import React from "react"

export default function Header({children}) {
  return (
    <header>
      <nav>
        <a href="/" title="Domů">[ Domů ]</a>
        {children}
      </nav>
    </header>
  )
}