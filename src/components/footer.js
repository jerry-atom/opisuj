import React from "react"

export default function Footer({children}) {
  return (
    <>
      <footer>
        {children}
        <p className="licence">
          <a
            rel="license"
            href="http://creativecommons.org/licenses/by-nc/4.0/deed.cs"
          >
            <img
              alt="Creative Commons License"
              style={{borderWidth:0}}
              src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png"
              width="80"
              height="15"
            />
          </a>
          <br />
          Obsah je licencovaný{' '}<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.cs">CC BY-NC 4.0</a>.
        </p>
      </footer>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169326121-1"></script>
      <script dangerouslySetInnerHTML={{__html:`
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-169326121-1');
`}} />
    </>
  );
}
