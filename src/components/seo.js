import React from "react"
import { Helmet } from "react-helmet"
import { useLocation } from "@reach/router"
import { useStaticQuery, graphql } from "gatsby"

export default function SEO({ title, description, image, article }) {
  const { pathname } = useLocation()
  const { site } = useStaticQuery(query)
  const seo = {
    description: description || site.siteMetadata.description,
    image: `${site.siteMetadata.siteUrl}${image || site.siteMetadata.image}`,
    url: `${site.siteMetadata.siteUrl}${pathname}`,
  }

  return (
    <Helmet
      title={title}
      titleTemplate={site.siteMetadata.titleTemplate}
      htmlAttributes={{lang: "cs"}}
    >
      <meta name="seznam-wmt" content="dh3VI2xkvclqNyZsLnnx3t7vOK9QaMqk" />
      <meta name="description" content={seo.description} />
      <meta name="image" content={seo.image} />
      {seo.url && <meta property="og:url" content={seo.url} />}
      {article && <meta property="og:type" content="article" />}
      {seo.title && <meta property="og:title" content={title} />}
      {seo.description && <meta property="og:description" content={seo.description} />}
      {seo.image && <meta property="og:image" content={seo.image} />}
    </Helmet>
  )
}

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        title
        description
        siteUrl
        image
      }
    }
  }
`