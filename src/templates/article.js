import { graphql } from "gatsby"
import React from "react"
import { Helmet } from "react-helmet"
import Content from "../components/content"
import Header from "../components/header"
import Footer from "../components/footer"
import SEO from "../components/seo"

export default function Template({
  data,
}) {
  const { markdownRemark } = data
  const { frontmatter, html } = markdownRemark
  const jsonLd = {
    "@context": "https://schema.org",
    "@type": "Article",
    "author": frontmatter.authorName,
    "image": "https://opisuj.cz/logo.jpg",
    "headline": frontmatter.writer + ", " + frontmatter.title,
    "keywords": frontmatter.keywords,
    "publisher": {
      "@type": "Organization",
      "name": "Opisuj",
      "logo": {
        "@type": "ImageObject",
        "url": "https://opisuj.cz/logo.jpg"
      }
    },
    "url": "https://opisuj.cz" + frontmatter.slug,
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": "https://opisuj.cz/"
    },
    "datePublished": frontmatter.datePublished ?? frontmatter.dateCreated,
    "dateCreated": frontmatter.dateCreated,
    "dateModified": frontmatter.dateModified ?? frontmatter.dateCreated,
    "description": frontmatter.description,
    "articleBody": html
  }
  return (
    <>
      <SEO
         title={frontmatter.writer + ": " + frontmatter.title}
         description={frontmatter.description}
         article={true}
      />
      <Helmet>
        <script type="application/ld+json">{JSON.stringify(jsonLd)}</script>
      </Helmet>
      <Header />
      <Content>
        <h1>{frontmatter.writer + ": " + frontmatter.title}</h1>
        <article dangerouslySetInnerHTML={{ __html: html }} />
      </Content>
      <Footer>
        <p>
          <i>Klíčová slova</i>: {frontmatter.keywords}<br/>
          <i>Popis</i>: {frontmatter.description}<br/>
          <i>Instituce</i>: {frontmatter.organization}<br/>
          <i>Datum</i>: {frontmatter.dateModified ?? frontmatter.datePublished ?? frontmatter.dateCreated}<br/>
          <i>Autor</i>: {frontmatter.authorName}
        </p>
      </Footer>
    </>
  )
}
export const pageQuery = graphql`
query($slug: String!) {
  markdownRemark(frontmatter: { slug: { eq: $slug } }) {
    frontmatter {
      authorName
      category
      description
      dateCreated
      dateModified
      datePublished
      keywords
      slug
      title
      writer
    }
    html
    rawMarkdownBody
  }
}
`
