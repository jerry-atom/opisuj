module.exports = {
  siteMetadata: {
    description: "Web se zaměřením na sdílení materiálů ke studiu, především pro základní a střední školy, učňáky, ale i jiné.",
    url: "https://opisuj.cz",
    image: "/images/icon.png",
    siteUrl: "https://opisuj.cz"
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    "gatsby-plugin-robots-txt",
    "gatsby-transformer-remark",
    "gatsby-plugin-postcss",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "markdown-pages",
        path: `${__dirname}/src/markdown-pages/`,
      },
    },
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "Opisuj seminárky, slohovky, maturitní okruhy, rozbory knih...",
        short_name: "Opisuj",
        start_url: "/",
        background_color: "#ffffff",
        theme_color: "#ffffff",
        display: "browser",
        icon: "src/images/icon.png",
        crossOrigin: "use-credentials",
      },
    }
  ]
}
